#!/usr/bin/env bash

user=
pass=
db=

for script in tables values messages views ;
do
    mysql -u${user} -p${pass} ${db} < db/${script}.sql
done

sudo rm -rf app/cache/* app/logs/*
curl -s https://getcomposer.org/installer | php
php composer.phar install --no-dev --optimize-autoloader
php app/console cache:clear --env=prod
php app/console assetic:dump --env=prod

rm -rf composer.phar
sudo rm -rf app/cache/* app/logs/*
chmod -R 777 app/cache/ app/logs/
