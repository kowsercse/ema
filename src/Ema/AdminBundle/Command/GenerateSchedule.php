<?php

namespace Ema\AdminBundle\Command;

use Doctrine\ORM\EntityManager;
use Ema\DomainBundle\Entity\User;
use Ema\DomainBundle\Entity\UserRepository;
use Ema\DomainBundle\Service\ScheduleGeneratorService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateSchedule extends ContainerAwareCommand {

  protected function configure()
  {
    $this
        ->setName('ema:generate:schedule')
        ->setDescription('Generate EMA schedule for participants')
        ->addOption('userId', 'u', InputOption::VALUE_OPTIONAL, 'The user id')
        ->addOption('studyId', null, InputOption::VALUE_OPTIONAL, 'The study id');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    /**
     * @var EntityManager $em
     * @var UserRepository $userRepository
     * @var ScheduleGeneratorService $scheduleGenerator
     * @var LoggerInterface $logger
     * @var User $user
     */
    $logger = $this->getContainer()->get('logger');
    $logger->debug("Schedule generation started.");
    $em = $this->getContainer()->get('doctrine')->getManager();
    $scheduleGenerator = $this->getContainer()->get('ema.schedule.generator');
    $userRepository = $em->getRepository('EmaDomainBundle:User');
    $users = $userRepository->getUsersRequireSchedule();
    foreach($users as $user) {
      if($user->getStartDate()->format("Y") < 2000) {
        $message = "Skipping schedule generation for [study_id=" . $user->getUsername() . "] with [start_date=" . $user->getStartDate()->format("Y-m-d") . "]";
        $logger->debug($message);
        $output->writeln($message);
        continue;
      }
      $message = "Generating schedule for [study_id=" . $user->getUsername() . "] with [start_date=" . $user->getStartDate()->format("Y-m-d") . "]";
      $logger->debug($message);
      $output->writeln($message);

      try {
        $scheduleGenerator->generateSchedule($user);
      }
      catch(\Exception $ex) {
        $this->printError($output, $user, $logger, $ex);
      }
    }
    $message = "Completed schedule generation for " . count($users) . " participants";
    $output->writeln($message);
    $logger->debug($message);
  }

  protected function printError(OutputInterface $output, User $user, LoggerInterface $logger, \Exception $ex)
  {
    $message = "Failed to generate schedule for participant [study_id=" . $user->getUsername() . "]";
    $logger->warning($message);
    $logger->warning($ex->getMessage());
    $logger->warning($ex->getTraceAsString());

    $output->writeln($message);
    $output->writeln($ex->getMessage());
    return $message;
  }
}
