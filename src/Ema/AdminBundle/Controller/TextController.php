<?php

namespace Ema\AdminBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class TextController extends Controller {

  public function showFormAction() {
    $text = $this->container->getParameter('clickatell.text');
    return $this->render('EmaAdminBundle:Text:form.html.twig', array(
        'text' => $text,
        'response' => null,
        'message' => null
    ));
  }

  public function postAction(Request $request) {
    $numberOne = $request->get('phone1');
    $numberTwo = $request->get('phone2');

    $message = "Failed to send text";
    $response = null;
    if ($numberOne && $numberTwo) {
      $response = $this->sendCurlRequest("+1" . $numberOne . $numberTwo);
      $message = $this->getMessage($response);
    }

    $text = $this->container->getParameter('clickatell.text');
    return $this->render('EmaAdminBundle:Text:form.html.twig', array(
        'text' => $text,
        'response' => $response,
        'message' => $message
    ));
  }

  protected function sendCurlRequest($number) {
    $requestBody = $this->createRequestBody($number);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->container->getParameter('clickatell.baseUri'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "data=" . $requestBody);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
  }

  /**
   * @return string
   */
  protected function createRequestBody($number) {
    $sequenceNumber = 'manual_' . date('Y_m_d_H_i_s');
    $apiId = $this->container->getParameter('clickatell.apiid');
    $username = $this->container->getParameter('clickatell.username');
    $password = $this->container->getParameter('clickatell.password');
    $from = $this->container->getParameter('clickatell.from');
    $text = $this->container->getParameter('clickatell.text');

    $body = '<?xml version="1.0"?>' .
        "<clickAPI>" .
        "  <sendMsg>" .
        "    <api_id>" . $apiId . "</api_id>" .
        "    <user>" . $username . "</user>" .
        "    <password>" . $password . "</password>" .
        "    <mo>1</mo>" .
        "    <from>" . $from . "</from>" .
        "    <to>" . $number . "</to>" .
        "    <text>" . $text . "</text>" .
        "    <sequence_no>" . $sequenceNumber . "</sequence_no>" .
        "    <concat>1</concat>" .
        "  </sendMsg>" .
        "</clickAPI>";

    return $body;
  }

  /**
   * @param $response
   * @return mixed
   */
  protected function getMessage($response) {
    $xml = new \SimpleXMLElement($response);
    if (isset($xml->sendMsgResp->fault)) {
      return "Failed: " . $xml->sendMsgResp->fault;
    }
    else if (isset($xml->sendMsgResp->apiMsgId)) {
      return "Text sent successfully to: " . $xml->sendMsgResp->to;
    }
    else {
      return "Failed to send text";
    }
  }

}
