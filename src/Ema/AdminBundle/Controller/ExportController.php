<?php

namespace Ema\AdminBundle\Controller;


use Ema\DomainBundle\Entity\Answer;
use Ema\DomainBundle\Entity\AnswerRepository;
use Ema\DomainBundle\Entity\Question;
use Ema\DomainBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;


class ExportController extends Controller {

  private $limit = 10;
  private $roleIndex = array(
    '0' => User::SS_STUDY_GROUP,
    '1' => User::BONING_UP_GROUP,
    '2' => User::PERSONAL_CHOICE_GROUP,
  );

  const DATETIME_FORMAT = 'Y-m-d H:i:s';
  const DATE_FORMAT = 'Y-m-d';

  public function showFormAction(Request $request) {
    /**
     * @var AnswerRepository $answerRepository
     */
    $today = new \DateTime();
    $today->setTime(0, 0, 0);

    $startDate = $request->query->get('startDate', $today);
    $endDate = $request->query->get('endDate', $today);
    $userRepository = $this->getDoctrine()->getManager()->getRepository('EmaDomainBundle:User');

    if ($request->query->get('export')) {
      $studyId = $request->query->get('studyId');
      $user = $userRepository->findByUsername($studyId);
      if($user) {
        $users = array($user);
      }
      else {
        $groupPage = $request->query->get('groupPage');
        $users = $this->retrieveUsers($groupPage);
      }
      if(count($users)) {
        return $this->sendCSVFile($startDate, $endDate, $users);
      }
    }

    $groupUsers = $this->retrieveGroupedUsers();
    return $this->render('EmaAdminBundle:Export:form.html.twig', array(
        'startDate' => $startDate,
        'endDate' => $endDate,
        'groupUsers' => $groupUsers,
        'roleIndex' => $this->roleIndex,
    ));
  }

  public function sendCSVFile($startDate, $endDate, array $studyIds) {
    /**
     * @var AnswerRepository $answerRepository
     * @var \DateTime $endTime
     */
    $endTime = $endDate instanceof \DateTime ? clone $endDate : new \DateTime($endDate);
    $endTime->modify('+1 day');
    $self = $this;

    $headers = $this->prepareResponseHeader();
    return new StreamedResponse(function () use ($startDate, $endTime, $studyIds, $self) {
      /**
       * @var Answer $answer
       * @var Answer $lastAnswer
       * @var AnswerRepository $answerRepository
       */
      echo $self->prepareHeader();
      echo "\n";

      $em = $self->getDoctrine()->getManager();
      $answerRepository = $em->getRepository('EmaDomainBundle:Answer');
      foreach ($studyIds as $studyId) {
        $values = array();
        $lastAnswer = null;
        $answers = $answerRepository->findAnswerBetween($startDate, $endTime, array($studyId));

        foreach ($answers as $answer) {
          if ($lastAnswer != null && !$self->isSameAnswerSet($answer, $lastAnswer)) {
            echo $self->prepareRow($values, $lastAnswer);
            $values = array();
          }
          $values[$answer->getQuestion()->getId()] = $answer->getAnswer();
          $lastAnswer = $answer;

          $em->detach($answer);
        }

        if($lastAnswer) {
          echo $self->prepareRow($values, $lastAnswer);
        }
      }
    }, Response::HTTP_OK, $headers);
  }

  /**
   * @param $values
   * @param Answer $lastAnswer
   * @return string
   */
  public function prepareRow($values, Answer $lastAnswer)
  {
    $row = "";
    $row .= $lastAnswer->getUser()->getUsername() . ",";
    $row .= $this->getDayOfStudy($lastAnswer) . ",";
    $row .= $lastAnswer->getSubmissionTime()->format(self::DATETIME_FORMAT) . ",";
    $row .= $lastAnswer->getUser()->getId() . ",";
    $row .= $lastAnswer->getUser()->getRole() . ",";
    $row .= $lastAnswer->getUser()->getStartDate()->format(self::DATETIME_FORMAT) . ",";
    if($lastAnswer->getSchedule()) {
      $row .= $lastAnswer->getSchedule()->getId() . ",";
      $row .= $lastAnswer->getSchedule()->getSurveyDate()->format(self::DATE_FORMAT) . ",";
    }
    else {
      $row .= ",,";
    }
    if($lastAnswer->getNotification()) {
      $row .= $lastAnswer->getNotification()->getId() . ",";
      $row .= $lastAnswer->getNotification()->getSerial() . ",";
      $row .= $lastAnswer->getNotification()->getScheduledTime()->format(self::DATETIME_FORMAT) . ",";
    }
    else {
      $row .= ",,,";
    }
    for ($i = 1; $i <= 32; $i++) {
      $row .= array_key_exists($i, $values) ? $values[$i] . "," : " ,";
    }
    $row .= "\n";

    return $row;
  }

  public function isSameAnswerSet(Answer $answer, Answer $lastAnswer)
  {
    if($lastAnswer->getUser()->getId() == $answer->getUser()->getId()
        && ($lastAnswer->getSubmissionTime() == $answer->getSubmissionTime())) {
      return true;
    }
    return false;
  }

  public function prepareHeader()
  {
    /**
     * @var Question $entity
     * @var Question $question
     */
    $em = $this->getDoctrine()->getManager();
    $entities = $em->getRepository('EmaDomainBundle:Question')->findAll();

    $questions = array();
    foreach($entities as $entity) {
      $questions[$entity->getId()] = $entity;
    }
    $header = "";

    $header .= "study_id,day_of_study,submitted_date,";
    $header .= "userId,role,startDate,";
    $header .= "scheduleId,surveyDate,";
    $header .= "notificationId,notificationSerial,scheduledTime,";
    for($i = 1; $i <= 32; $i++) {
      $question = $questions[$i];
      $header .= $question->getTitle() . ",";
    }

    return $header;
  }

  /**
   * @param Answer $lastAnswer
   * @return int The day of study
   */
  public function getDayOfStudy(Answer $lastAnswer)
  {
    $submissionTime = clone $lastAnswer->getSubmissionTime();
    $startDate = clone $lastAnswer->getUser()->getStartDate();

    $submissionTime->setTime(0, 0, 0);
    $startDate->setTime(0, 0, 0);

    $dateInterval = $submissionTime->diff($startDate);
    return $dateInterval->days;
  }

  private function prepareResponseHeader() {
    $today = new \DateTime();
    $fileName = "ema_" . $today->format('Y_m_d_H_i') . "_0.csv";
    $headers = array('Content-Type' => 'text/csv', 'Content-Disposition' => 'attachment; filename="' . $fileName . '"');
    return $headers;
  }

  /**
   * @param $groupPage
   * @return mixed
   */
  private function retrieveUsers($groupPage) {
    $explode = explode("_", $groupPage);
    if(count($explode) != 2) {
      return null;
    }

    list($group, $page) = $explode;
    $role = $this->roleIndex[$group];
    $offset = $page * $this->limit;
    $userRepository = $this->getDoctrine()->getManager()->getRepository('EmaDomainBundle:User');
    $users = $userRepository->findUsersByRole($role, $this->limit, $offset);
    return $users;
  }

  /**
   * @return array
   */
  protected function retrieveGroupedUsers() {
    $groupUsers = array();
    $userRepository = $this->getDoctrine()->getManager()->getRepository('EmaDomainBundle:User');
    foreach ($this->roleIndex as $role) {
      $users = $userRepository->findUsersByRole($role, 10000);
      $totalGroupUser = count($users);
      $completeRange = floor($totalGroupUser / $this->limit);

      $ranges = array();
      for ($i = 0; $i < $completeRange; $i++) {
        $ranges[] = array(
            'first' => $users[$i * $this->limit]->getUsername(),
            'last' => $users[($i + 1) * $this->limit - 1]->getUsername(),
        );
      }
      if ($completeRange * $this->limit < $totalGroupUser) {
        $ranges[] = array(
            'first' => $users[$completeRange * $this->limit]->getUsername(),
            'last' => end($users)->getUsername(),
        );
      }

      $groupUsers[] = $ranges;
    }
    return $groupUsers;
  }

}
