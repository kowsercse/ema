<?php

namespace Ema\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Ema\DomainBundle\Entity\ContactingTime;
use Ema\AdminBundle\Form\ContactingTimeType;

/**
 * ContactingTime controller.
 *
 */
class ContactingTimeController extends Controller
{

    /**
     * Lists all ContactingTime entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EmaDomainBundle:ContactingTime')->findAll();

        return $this->render('EmaAdminBundle:ContactingTime:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ContactingTime entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ContactingTime();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_contactingtime_show', array('id' => $entity->getId())));
        }

        return $this->render('EmaAdminBundle:ContactingTime:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a ContactingTime entity.
    *
    * @param ContactingTime $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(ContactingTime $entity)
    {
        $form = $this->createForm(new ContactingTimeType(), $entity, array(
            'action' => $this->generateUrl('admin_contactingtime_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ContactingTime entity.
     *
     */
    public function newAction()
    {
        $entity = new ContactingTime();
        $form   = $this->createCreateForm($entity);

        return $this->render('EmaAdminBundle:ContactingTime:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ContactingTime entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EmaDomainBundle:ContactingTime')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactingTime entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EmaAdminBundle:ContactingTime:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing ContactingTime entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EmaDomainBundle:ContactingTime')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactingTime entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EmaAdminBundle:ContactingTime:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ContactingTime entity.
    *
    * @param ContactingTime $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ContactingTime $entity)
    {
        $form = $this->createForm(new ContactingTimeType(), $entity, array(
            'action' => $this->generateUrl('admin_contactingtime_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ContactingTime entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EmaDomainBundle:ContactingTime')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactingTime entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_contactingtime_edit', array('id' => $id)));
        }

        return $this->render('EmaAdminBundle:ContactingTime:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ContactingTime entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EmaDomainBundle:ContactingTime')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ContactingTime entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_contactingtime'));
    }

    /**
     * Creates a form to delete a ContactingTime entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_contactingtime_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
