<?php
/**
 * Created by PhpStorm.
 * User: kowser
 * Date: 3/10/14
 * Time: 8:55 PM
 */

namespace Ema\AdminBundle\Controller;


use Doctrine\ORM\EntityManager;
use Ema\DomainBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ScheduleGeneratorController extends Controller{

  public function indexAction() {
    return $this->render('EmaAdminBundle:ScheduleGenerator:index.html.twig');
  }

  public function showSchedulesAction($id) {
    $em = $this->getDoctrine()->getManager();

    $user = $em->getRepository('EmaDomainBundle:User')->findOneBy(array('username'=>$id));
    if(!$user) {
      throw $this->createNotFoundException("Can not find user with id: ".$id);
    }
    $schedules = $em->getRepository('EmaDomainBundle:Schedule')->findByUser($user);

    return $this->render('EmaAdminBundle:ScheduleGenerator:show.html.twig', array(
        'schedules' => $schedules,
        'user' => $user
    ));
  }

  public function generateAction(Request $request) {
    /**
     * @var EntityManager $em
     */
    $em = $this->getDoctrine()->getManager();
    $studyId = $request->get('userId');
    $user = $em->getRepository('EmaDomainBundle:User')->findOneBy(array('username'=>$studyId));
    if(!$user) {
      throw $this->createNotFoundException("User do not exists with studyId: " . $studyId);
    }
    try {
      $this->get('ema.schedule.generator')->generateSchedule($user);
    }
    catch(\Exception $ex) {
      echo $ex->getMessage();
    }

    return new Response();
  }

}
