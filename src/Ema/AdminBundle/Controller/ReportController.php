<?php

namespace Ema\AdminBundle\Controller;


use Ema\DomainBundle\Entity\Notification;
use Ema\DomainBundle\Entity\Schedule;
use Ema\DomainBundle\Entity\ScheduleRepository;
use Ema\DomainBundle\Entity\User;
use Ema\DomainBundle\Entity\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ReportController extends Controller {

  public function showNotificationStatusAction(Request $request) {
    /**
     * @var ScheduleRepository $scheduleRepository
     * @var UserRepository $userRepository
     * @var User $user
     */
    $today = new \DateTime();
    $today->setTime(0, 0, 0);

    $startDate = $request->query->get('startDate', $today);
    $endDate = $request->query->get('endDate', $today);
    $studyId = $request->query->get('studyId');
    $extra = array();

    if($endDate < $startDate) {
      $result = array();
    }
    else {
      $scheduleRepository = $this->getDoctrine()->getManager()->getRepository('EmaDomainBundle:Schedule');
      $userRepository = $this->getDoctrine()->getManager()->getRepository('EmaDomainBundle:User');
      $result = $scheduleRepository->findSchedulesBetween($startDate, $endDate, $studyId);
      if($studyId) {
        $user = $userRepository->findByUsername($studyId);
        if($user) {
          $baselineRow = $userRepository->getBaselineRow($user);
          if($user->getPreviousRole() != "Personal Choice Group") {
            $firstGiftCardEndDate = clone $user->getStartDate();
            $firstGiftCardEndDate->modify("+84 day");
          }
          else {
            $firstGiftCardEndDate = clone $user->getStartDate();
            $firstGiftCardEndDate->modify("+280 day");
          }
          $surveyDates = array();
          for($i = 1; $i <= 3; $i++) {
            $participantSurveyDate = clone $user->getStartDate();
            $participantSurveyDate->modify("+". ($i * 90 + 1) . " day");
            $surveyDates[] = $participantSurveyDate;
          }
          $participantSurveyDate = clone $user->getStartDate();
          $participantSurveyDate->modify("+". (345) . " day");
          $surveyDates[] = $participantSurveyDate;

          $scheduleGeneratorService = $this->get('ema.schedule.generator');
          $extra = array(
              'baseline' => $baselineRow,
              'user' => $user,
              'firstGiftCardEndDate' => $firstGiftCardEndDate,
              'totalSubmitted' => $scheduleGeneratorService->getTotalSubmitted($user),
              'totalDenied' => $scheduleGeneratorService->getTotalDenied($user),
              'totalSchedule' => $scheduleGeneratorService->getTotalScheduleBetween($user),
              'progress' => $scheduleGeneratorService->getProgress($user),
              'surveyDates' => $surveyDates,
          );
        }
      }
    }

    if($request->query->get('export')) {
      return $this->sendCSVFile($result);
    }
    else {
      $data = array(
          'startDate' => $startDate,
          'endDate' => $endDate,
          'result' => $result,
      );
      return $this->render('EmaAdminBundle:Report:form.html.twig', array_merge($data, $extra));
    }
  }

  /**
   * @return string
   */
  protected function createExportFilename() {
    $today = new \DateTime();
    $fileName = "notifications_" . $today->format('Y_m_d_H_i') . "_0.csv";
    return $fileName;
  }

  /**
   * @param $result
   * @return \Symfony\Component\HttpFoundation\Response
   */
  protected function sendCSVFile($result) {
    $self = $this;
    return new StreamedResponse(function() use ($result, $self){
      /**
       * @var Schedule $schedule
       * @var Notification $notification;
       */
      echo $self->prepareHeader() . "\n";

      foreach($result as $schedule) {
        if ($schedule->getNotifications()->count()) {
          foreach($schedule->getNotifications() as $notification) {
            $line = $schedule->getUser()->getUsername() . "," . $schedule->getSurveyDate()->format('Y-m-d') . ","
                . $notification->getScheduledTime()->format('Y-m-d H:i:s') . ","
                . $notification->getSerial() . ","
                . ($notification->getSent() ? 'Yes' : 'No') . ",";
            if($notification->getAnswers()->count()) {
              $collection = $notification->getAnswers();
              $line .= "1," . $collection[0]->getSubmissionTime()->format("Y-m-d H:i:s");
            }
            else {
              $line .= "0";
            }
            echo $line . "\n";
          }
        }
        else {
          $line = $schedule->getUser()->getUsername() . "," . $schedule->getSurveyDate()->format('Y-m-d') . ",Not generated";
          echo $line . "\n";
        }
      }
    }, Response::HTTP_OK, array('Content-Type'=>'text/csv', 'Content-Disposition' => 'attachment; filename="'.$this->createExportFilename().'"'));
  }

  public function prepareHeader() {
    return "Study Id,Survey Date,Notification Time,Notification Serial,Notification Sent,Participant Answered,Submission Time";
  }

}
