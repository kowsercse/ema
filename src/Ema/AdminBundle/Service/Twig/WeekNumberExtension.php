<?php

namespace Ema\AdminBundle\Service\Twig;


use Ema\DomainBundle\Entity\Schedule;

class WeekNumberExtension extends \Twig_Extension {

  public function getFilters() {
    return array(
        new \Twig_SimpleFilter('weekNumber', array($this, 'filter')),
    );
  }

  public function filter(Schedule $schedule) {
    $startDate = clone $schedule->getUser()->getStartDate();
    $startDate->setTime(0, 0);

    $dateInterval = $schedule->getSurveyDate()->diff($startDate);
    return (int) ((($dateInterval->days - 1) / 7) + 1);
  }

  public function getName() {
    return 'ema_extension';
  }

}
