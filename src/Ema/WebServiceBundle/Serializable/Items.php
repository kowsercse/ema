<?php

namespace Ema\WebServiceBundle\Serializable;


class Items {

  private $answer;

  /**
   * @param mixed $answer
   */
  public function setAnswer($answer)
  {
    $this->answer = $answer;
  }

  /**
   * @return mixed
   */
  public function getAnswer()
  {
    return $this->answer;
  }

}
