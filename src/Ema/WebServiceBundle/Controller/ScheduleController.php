<?php

namespace Ema\WebServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;

/**
 * Schedule controller.
 *
 */
class ScheduleController extends ResourceController
{
  function __construct()
  {
    parent::__construct();
  }

  /**
   * Lists all Schedule entities.
   *
   */
  public function indexAction()
  {
    $em = $this->getDoctrine()->getManager();

    $entities = $em->getRepository('EmaDomainBundle:Schedule')->findAll();

    $response = $this->render('EmaWebServiceBundle:Schedule:list.xml.twig', array(
        'entities' => $entities,
    ));

    $response->headers->set("Content-Type", "application/xml");
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Creates a new Schedule entity.
   *
   */
  public function createAction(Request $request)
  {
    $schedule = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\Schedule',
        'xml'
    );

    $em = $this->getDoctrine()->getManager();
    $em->persist($schedule);
    $em->flush();

    $uri = $this->get('router')->generate('webservice_schedule_show', array('id' => $schedule->getId()), Router::ABSOLUTE_URL);
    $response = new Response('', Response::HTTP_CREATED, array(
        'Location' => $uri,
        'Content-Type' => 'application/xml'
    ));
    return $response;
  }

  /**
   * Finds and displays a Schedule entity.
   *
   */
  public function showAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:Schedule')->find($id);
    if (!$entity) {
      throw $this->createNotFoundException('Unable to find schedule entity.');
    }

    $response = $this->render('EmaWebServiceBundle:Schedule:show.xml.twig', array('entity' => $entity));
    $response->setStatusCode(Response::HTTP_OK);
    $response->headers->set("Content-Type", "application/xml");
    return $response;
  }

  /**
   * Edits an existing Schedule entity.
   *
   */
  public function updateAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:Schedule')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find schedule entity.');
    }

    $schedule = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\Schedule',
        'xml'
    );
    $entity->setFirstName($schedule->getFirstName());
    $entity->setLastName($schedule->getLastName());
    $entity->setUser($schedule->getUser());

    $em->merge($entity);
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Deletes a Schedule entity.
   *
   */
  public function deleteAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:Schedule')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find schedule entity.');
    }

    $em->remove($entity);
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

}
