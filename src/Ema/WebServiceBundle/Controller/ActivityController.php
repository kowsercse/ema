<?php

namespace Ema\WebServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;

/**
 * Activity controller.
 *
 */
class ActivityController extends ResourceController
{
  function __construct()
  {
    parent::__construct();
  }

  /**
   * Lists all Activity entities.
   *
   */
  public function indexAction()
  {
    $em = $this->getDoctrine()->getManager();

    $entities = $em->getRepository('EmaDomainBundle:Activity')->findAll();

    $response = $this->render('EmaWebServiceBundle:Activity:list.xml.twig', array(
        'entities' => $entities,
    ));

    $response->headers->set("Content-Type", "application/xml");
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Creates a new Activity entity.
   *
   */
  public function createAction(Request $request)
  {
    $activity = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\Activity',
        'xml'
    );

    $em = $this->getDoctrine()->getManager();
    $em->persist($activity);
    $em->flush();

    $uri = $this->get('router')->generate('webservice_activity_show', array('id' => $activity->getId()), Router::ABSOLUTE_URL);
    $response = new Response('', Response::HTTP_CREATED, array(
        'Location' => $uri,
        'Content-Type' => 'application/xml'
    ));
    return $response;
  }

  /**
   * Finds and displays a Activity entity.
   *
   */
  public function showAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:Activity')->find($id);
    if (!$entity) {
      throw $this->createNotFoundException('Unable to find activity entity.');
    }

    $response = $this->render('EmaWebServiceBundle:Activity:show.xml.twig', array('entity' => $entity));
    $response->setStatusCode(Response::HTTP_OK);
    $response->headers->set("Content-Type", "application/xml");
    return $response;
  }

  /**
   * Edits an existing Activity entity.
   *
   */
  public function updateAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:Activity')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find activity entity.');
    }

    $activity = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\Activity',
        'xml'
    );
    $entity->setFirstName($activity->getFirstName());
    $entity->setLastName($activity->getLastName());
    $entity->setUser($activity->getUser());

    $em->merge($entity);
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Deletes a Activity entity.
   *
   */
  public function deleteAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:Activity')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find activity entity.');
    }

    $em->remove($entity);
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

}
