<?php

namespace EMA\WebServiceBundle\Controller;

use Ema\DomainBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;

class EndpointController extends Controller
{

  public function indexAction()
  {
    /**
     * @var User $user
     */
    $user = $this->getUser();
    $response = $this->render('EmaWebServiceBundle:Endpoint:index.xml.twig', array(
      'user' => $user,
      'contactingTime' => $user->getContactingTime()
    ));

    $response->headers->set("Content-Type", "application/xml");
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

}
