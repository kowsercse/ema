<?php

namespace Ema\WebServiceBundle\Controller;

use Ema\DomainBundle\Entity\ContactingTime;
use Ema\DomainBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;

/**
 * ContactingTime controller.
 *
 */
class ContactingTimeController extends ResourceController
{
  public function ownerUpdateAction(Request $request)
  {
    /**
     * @var User $user
     * @var ContactingTime $contactingTime
     */
    $user = $this->getUser();
    $contactingTime = $user->getContactingTime();
    $em = $this->getDoctrine()->getManager();

    $serializedTime = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\ContactingTime',
        'xml'
    );

    $startTime = \DateTime::createFromFormat("H:i:s", $serializedTime->getStartTime());
    $minute = $startTime->format('i');
    if(!in_array($minute, array(0, 15, 30, 45))) {
      throw new \InvalidArgumentException();
    }
    $startTime->setTime($startTime->format('H'), $minute, 0);

    if($contactingTime) {
      $contactingTime->setStartTime($startTime);
      $em->merge($contactingTime);
    }
    else {
      $contactingTime = new ContactingTime();
      $contactingTime->setUser($user);
      $contactingTime->setStartTime($startTime);
      $em->persist($contactingTime);
    }
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Lists all ContactingTime entities.
   *
   */
  public function indexAction()
  {
    $em = $this->getDoctrine()->getManager();

    $entities = $em->getRepository('EmaDomainBundle:ContactingTime')->findAll();

    $response = $this->render('EmaWebServiceBundle:ContactingTime:list.xml.twig', array(
        'entities' => $entities,
    ));

    $response->headers->set("Content-Type", "application/xml");
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Creates a new ContactingTime entity.
   *
   */
  public function createAction(Request $request)
  {
    $contactingtime = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\ContactingTime',
        'xml'
    );

    $em = $this->getDoctrine()->getManager();
    $em->persist($contactingtime);
    $em->flush();

    $uri = $this->get('router')->generate('webservice_contactingtime_show', array('id' => $contactingtime->getId()), Router::ABSOLUTE_URL);
    $response = new Response('', Response::HTTP_CREATED, array(
        'Location' => $uri,
        'Content-Type' => 'application/xml'
    ));
    return $response;
  }

  /**
   * Finds and displays a ContactingTime entity.
   *
   */
  public function showAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:ContactingTime')->find($id);
    if (!$entity) {
      throw $this->createNotFoundException('Unable to find contactingtime entity.');
    }

    $response = $this->render('EmaWebServiceBundle:ContactingTime:show.xml.twig', array('entity' => $entity));
    $response->setStatusCode(Response::HTTP_OK);
    $response->headers->set("Content-Type", "application/xml");
    return $response;
  }

  /**
   * Edits an existing ContactingTime entity.
   *
   */
  public function updateAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:ContactingTime')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find contactingtime entity.');
    }

    $contactingtime = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\ContactingTime',
        'xml'
    );
    $entity->setFirstName($contactingtime->getFirstName());
    $entity->setLastName($contactingtime->getLastName());
    $entity->setUser($contactingtime->getUser());

    $em->merge($entity);
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Deletes a ContactingTime entity.
   *
   */
  public function deleteAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:ContactingTime')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find contactingtime entity.');
    }

    $em->remove($entity);
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  private function overlaps(ContactingTime $contactingTime, \DateTime $startTime)
  {
    if($contactingTime == null) {
      return false;
    }

    $contactStartTime = $contactingTime->getStartTime();

    $newTime = new \DateTime();
    $newTime->setTime($startTime->format("H"), $startTime->format('i'), 0);

    $start = new \DateTime();
    $start->setTime($contactStartTime->format("H"), $contactStartTime->format("H"), 0);
    $end = clone $start;
    $end->modify("+10 hours");

    return $newTime >= $start && $newTime <= $end;
  }

}
