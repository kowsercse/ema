<?php

namespace Ema\WebServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;

/**
 * Question controller.
 *
 */
class QuestionController extends ResourceController
{
  function __construct()
  {
    parent::__construct();
  }

  /**
   * Lists all Question entities.
   *
   */
  public function indexAction()
  {
    $em = $this->getDoctrine()->getManager();

    $entities = $em->getRepository('EmaDomainBundle:Question')->findAll();

    $response = $this->render('EmaWebServiceBundle:Question:list.xml.twig', array(
        'entities' => $entities,
    ));

    $response->headers->set("Content-Type", "application/xml");
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Creates a new Question entity.
   *
   */
  public function createAction(Request $request)
  {
    $question = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\Question',
        'xml'
    );

    $em = $this->getDoctrine()->getManager();
    $em->persist($question);
    $em->flush();

    $uri = $this->get('router')->generate('webservice_question_show', array('id' => $question->getId()), Router::ABSOLUTE_URL);
    $response = new Response('', Response::HTTP_CREATED, array(
        'Location' => $uri,
        'Content-Type' => 'application/xml'
    ));
    return $response;
  }

  /**
   * Finds and displays a Question entity.
   *
   */
  public function showAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:Question')->find($id);
    if (!$entity) {
      throw $this->createNotFoundException('Unable to find question entity.');
    }

    $response = $this->render('EmaWebServiceBundle:Question:show.xml.twig', array('entity' => $entity));
    $response->setStatusCode(Response::HTTP_OK);
    $response->headers->set("Content-Type", "application/xml");
    return $response;
  }

  /**
   * Edits an existing Question entity.
   *
   */
  public function updateAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:Question')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find question entity.');
    }

    $question = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\Question',
        'xml'
    );
    $entity->setFirstName($question->getFirstName());
    $entity->setLastName($question->getLastName());
    $entity->setUser($question->getUser());

    $em->merge($entity);
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Deletes a Question entity.
   *
   */
  public function deleteAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:Question')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find question entity.');
    }

    $em->remove($entity);
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

}
