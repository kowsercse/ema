<?php

namespace Ema\WebServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;

/**
 * Answer controller.
 *
 */
class AnswerController extends ResourceController
{
  function __construct()
  {
    parent::__construct();
  }

  /**
   * Lists all Answer entities.
   *
   */
  public function indexAction()
  {
    $em = $this->getDoctrine()->getManager();

    $entities = $em->getRepository('EmaDomainBundle:Answer')->findAll();

    $response = $this->render('EmaWebServiceBundle:Answer:list.xml.twig', array(
        'entities' => $entities,
    ));

    $response->headers->set("Content-Type", "application/xml");
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Creates a new Answer entity.
   *
   */
  public function createAction(Request $request)
  {
    $answer = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\Answer',
        'xml'
    );

    $em = $this->getDoctrine()->getManager();
    $em->persist($answer);
    $em->flush();

    $uri = $this->get('router')->generate('webservice_answer_show', array('id' => $answer->getId()), Router::ABSOLUTE_URL);
    $response = new Response('', Response::HTTP_CREATED, array(
        'Location' => $uri,
        'Content-Type' => 'application/xml'
    ));
    return $response;
  }

  /**
   * Finds and displays a Answer entity.
   *
   */
  public function showAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:Answer')->find($id);
    if (!$entity) {
      throw $this->createNotFoundException('Unable to find answer entity.');
    }

    $response = $this->render('EmaWebServiceBundle:Answer:show.xml.twig', array('entity' => $entity));
    $response->setStatusCode(Response::HTTP_OK);
    $response->headers->set("Content-Type", "application/xml");
    return $response;
  }

  /**
   * Edits an existing Answer entity.
   *
   */
  public function updateAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:Answer')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find answer entity.');
    }

    $answer = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\Answer',
        'xml'
    );
    $entity->setFirstName($answer->getFirstName());
    $entity->setLastName($answer->getLastName());
    $entity->setUser($answer->getUser());

    $em->merge($entity);
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Deletes a Answer entity.
   *
   */
  public function deleteAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:Answer')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find answer entity.');
    }

    $em->remove($entity);
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

}
