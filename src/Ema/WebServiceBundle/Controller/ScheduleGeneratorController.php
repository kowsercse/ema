<?php
/**
 * Created by PhpStorm.
 * User: kowser
 * Date: 4/22/14
 * Time: 2:10 AM
 */

namespace Ema\WebServiceBundle\Controller;


use Ema\DomainBundle\Entity\ScheduleRepository;
use Ema\DomainBundle\Entity\User;
use Ema\DomainBundle\Service\ScheduleGeneratorService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ScheduleGeneratorController extends Controller
{

  public function generateAction(Request $request, $id)
  {
    /**
     * @var ScheduleRepository $scheduleRepository
     * @var ScheduleGeneratorService $scheduleGenerator
     * @var User $user
     * @var LoggerInterface $logger
     */
    $logger = $this->get('logger');
    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository('EmaDomainBundle:User')->find($id);
    if ($user) {
      $scheduleRepository = $this->getDoctrine()->getManager()->getRepository("EmaDomainBundle:Schedule");
      $totalSchedule = $scheduleRepository->findTotalScheduleBy($user);
      if($totalSchedule <= 0) {
        $logger->debug("Generating EMA schedule for user with id: " . $id . " study_id: " . $user->getUsername());
        $scheduleGenerator = $this->get('ema.schedule.generator');
        $scheduleGenerator->generateSchedule($user);
        $logger->debug("Total 280 EMA schedule generated for user with id: " . $id . " study_id: " . $user->getUsername());
        return new Response("", Response::HTTP_OK);
      }
      else {
        $logger->error("EMA schedule is already generated for user with id: " . $id . " study_id: " . $user->getUsername());
        return new Response("", Response::HTTP_PRECONDITION_FAILED);
      }
    }

    $logger->error("User not found with id: " . $id);
    return new Response("User not found with id: " . $id, Response::HTTP_NOT_FOUND);
  }

}
