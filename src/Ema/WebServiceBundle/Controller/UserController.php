<?php

namespace Ema\WebServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;

/**
 * User controller.
 *
 */
class UserController extends ResourceController
{
  function __construct()
  {
    parent::__construct();
  }

  /**
   * Lists all User entities.
   *
   */
  public function indexAction()
  {
    $em = $this->getDoctrine()->getManager();

    $entities = $em->getRepository('EmaDomainBundle:User')->findAll();

    $response = $this->render('EmaWebServiceBundle:User:list.xml.twig', array(
        'entities' => $entities,
    ));

    $response->headers->set("Content-Type", "application/xml");
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Creates a new User entity.
   *
   */
  public function createAction(Request $request)
  {
    $user = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\User',
        'xml'
    );

    $em = $this->getDoctrine()->getManager();
    $em->persist($user);
    $em->flush();

    $uri = $this->get('router')->generate('webservice_user_show', array('id' => $user->getId()), Router::ABSOLUTE_URL);
    $response = new Response('', Response::HTTP_CREATED, array(
        'Location' => $uri,
        'Content-Type' => 'application/xml'
    ));
    return $response;
  }

  /**
   * Finds and displays a User entity.
   *
   */
  public function showAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:User')->find($id);
    if (!$entity) {
      throw $this->createNotFoundException('Unable to find user entity.');
    }

    $response = $this->render('EmaWebServiceBundle:User:show.xml.twig', array('entity' => $entity));
    $response->setStatusCode(Response::HTTP_OK);
    $response->headers->set("Content-Type", "application/xml");
    return $response;
  }

  /**
   * Edits an existing User entity.
   *
   */
  public function updateAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:User')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find user entity.');
    }

    $user = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\DomainBundle\Entity\User',
        'xml'
    );
    $entity->setFirstName($user->getFirstName());
    $entity->setLastName($user->getLastName());
    $entity->setUser($user->getUser());

    $em->merge($entity);
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

  /**
   * Deletes a User entity.
   *
   */
  public function deleteAction(Request $request, $id)
  {
    $em = $this->getDoctrine()->getManager();
    $entity = $em->getRepository('EmaDomainBundle:User')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find user entity.');
    }

    $em->remove($entity);
    $em->flush();

    $response = new Response();
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
  }

}
