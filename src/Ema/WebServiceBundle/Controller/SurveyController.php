<?php

namespace Ema\WebServiceBundle\Controller;

use Ema\DomainBundle\Entity\Answer;
use Ema\DomainBundle\Entity\Notification;
use Ema\DomainBundle\Service\SurveyService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SurveyController extends ResourceController
{

  public function getActiveNotificationAction() {
    $currentNotification = $this->getCurrentNotification();
    if($currentNotification) {
      $response = $this->render('EmaWebServiceBundle:Notification:show.xml.twig', array(
          'entity' => $currentNotification
      ));

      $response->headers->set("Content-Type", "application/xml");
      $response->setStatusCode(Response::HTTP_OK);
      return $response;
    }

    throw $this->createNotFoundException("No current notification found");
  }

  public function submitAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $questions = $this->getQuestions($em);
    $serializedAnswers = $this->serializer->deserialize(
        $request->getContent(),
        'Ema\WebServiceBundle\Serializable\Items',
        'xml'
    );

    $answers = $this->prepareAnswers($serializedAnswers, $questions);
    foreach ($answers as $answer) {
      $em->persist($answer);
    }
    $em->flush();

    return new Response("", Response::HTTP_OK);
  }

  public function noUpdateAction() {
    $currentNotification = $this->getCurrentNotification();
    if($currentNotification == null) {
      throw $this->createNotFoundException("No active survey found");
    }
    $schedule = $currentNotification->getSchedule();
    $schedule->setDenied(true);
    $em = $this->getDoctrine()->getManager();
    $em->merge($schedule);
    $em->flush();

    return new Response("", Response::HTTP_OK);
  }

  public function getProgressAction() {
    $user = $this->getUser();
    $progress = $this->get('ema.schedule.generator')->getProgress($user);
    $response = $this->render('EmaWebServiceBundle:Survey:progress.xml.twig', array(
        'progress' => $progress
    ));

    $response->setStatusCode(Response::HTTP_OK);
    $response->headers->set("Content-Type", "application/xml");
    return $response;
  }

  /**
   * @param $serializedAnswers
   * @param $questions
   * @return array
   */
  private function prepareAnswers($serializedAnswers, $questions)
  {
    $currentNotification = $this->getCurrentNotification();
    $now = new \DateTime();
    $user = $this->getUser();
    $answers = array();
    foreach ($serializedAnswers->getAnswer() as $serializedAnswer) {
      if (array_key_exists($serializedAnswer['question'], $questions)) {
        $answer = new Answer();
        $answer->setQuestion($questions[$serializedAnswer['question']]);
        $answer->setAnswer($serializedAnswer['value']);
        $answer->setSubmissionTime($now);
        $answer->setUser($user);
        if($currentNotification != null) {
          $answer->setNotification($currentNotification);
          $answer->setSchedule($currentNotification->getSchedule());
        }

        $answers[$serializedAnswer['question']] = $answer;
      }
    }
    return $answers;
  }

  /**
   * @param $em
   * @return array
   */
  private function getQuestions($em)
  {
    $questions = $em->getRepository('EmaDomainBundle:Question')->findAll();
    $map = array();
    foreach ($questions as $question) {
      $map[$question->getId()] = $question;
    }
    return $map;
  }

  /**
   * @return Notification|null
   */
  private function getCurrentNotification() {
    $logger = $this->get('logger');
    $user = $this->getUser();
    /**
     * @var SurveyService $surveyService
     */
    $surveyService = $this->get('ema.survey.service');
    $notification = $surveyService->getCurrentNotification($user);
    if($notification != null) {
      $logger->info("Found current notification: ".$notification." for user: " . $user);
      $schedule = $notification->getSchedule();
      return $schedule->getDenied() || count($schedule->getAnswers()) > 0 ? null : $notification;
    }

    $logger->info("No current notification found for user: " . $user);
    return null;
  }

}
