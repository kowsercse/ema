<?php

namespace Ema\DomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 */
class Question
{
  /**
   * @var integer
   */
  private $id;

  /**
   * @var string
   */
  private $title;

  /**
   * @var \Ema\DomainBundle\Entity\Activity
   */
  private $activity;


  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set title
   *
   * @param string $title
   * @return Question
   */
  public function setTitle($title)
  {
    $this->title = $title;

    return $this;
  }

  /**
   * Get title
   *
   * @return string
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * Set activity
   *
   * @param \Ema\DomainBundle\Entity\Activity $activity
   * @return Question
   */
  public function setActivity(\Ema\DomainBundle\Entity\Activity $activity)
  {
    $this->activity = $activity;

    return $this;
  }

  /**
   * Get activity
   *
   * @return \Ema\DomainBundle\Entity\Activity
   */
  public function getActivity()
  {
    return $this->activity;
  }
}
