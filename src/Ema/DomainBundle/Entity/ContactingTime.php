<?php

namespace Ema\DomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContactingTime
 */
class ContactingTime
{
  /**
   * @var integer
   */
  private $id;

  /**
   * @var \DateTime
   */
  private $startTime;

  /**
   * @var \Ema\DomainBundle\Entity\User
   */
  private $user;


  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set startTime
   *
   * @param \DateTime $startTime
   * @return ContactingTime
   */
  public function setStartTime($startTime)
  {
    $this->startTime = $startTime;

    return $this;
  }

  /**
   * Get startTime
   *
   * @return \DateTime
   */
  public function getStartTime()
  {
    return $this->startTime;
  }

  /**
   * Set user
   *
   * @param \Ema\DomainBundle\Entity\User $user
   * @return ContactingTime
   */
  public function setUser(\Ema\DomainBundle\Entity\User $user)
  {
    $this->user = $user;

    return $this;
  }

  /**
   * Get user
   *
   * @return \Ema\DomainBundle\Entity\User
   */
  public function getUser()
  {
    return $this->user;
  }
}
