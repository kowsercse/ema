<?php

namespace Ema\DomainBundle\Entity;


class Schedule
{
  /**
   * @var \DateTime
   */
  private $surveyDate;

  /**
   * @var boolean
   */
  private $denied;

  /**
   * @var integer
   */
  private $id;

  /**
   * @var \Doctrine\Common\Collections\Collection
   */
  private $answers;

  /**
   * @var \Doctrine\Common\Collections\Collection
   */
  private $notifications;

  /**
   * @var \Ema\DomainBundle\Entity\User
   */
  private $user;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    $this->notifications = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Set surveyDate
   *
   * @param \DateTime $surveyDate
   * @return Schedule
   */
  public function setSurveyDate($surveyDate)
  {
    $this->surveyDate = $surveyDate;

    return $this;
  }

  /**
   * Get surveyDate
   *
   * @return \DateTime
   */
  public function getSurveyDate()
  {
    return $this->surveyDate;
  }

  /**
   * Set denied
   *
   * @param boolean $denied
   * @return Schedule
   */
  public function setDenied($denied)
  {
    $this->denied = $denied;

    return $this;
  }

  /**
   * Get denied
   *
   * @return boolean
   */
  public function getDenied()
  {
    return $this->denied;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Add answers
   *
   * @param \Ema\DomainBundle\Entity\Answer $answers
   * @return Schedule
   */
  public function addAnswer(\Ema\DomainBundle\Entity\Answer $answers)
  {
    $this->answers[] = $answers;

    return $this;
  }

  /**
   * Remove answers
   *
   * @param \Ema\DomainBundle\Entity\Answer $answers
   */
  public function removeAnswer(\Ema\DomainBundle\Entity\Answer $answers)
  {
    $this->answers->removeElement($answers);
  }

  /**
   * Get answers
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getAnswers()
  {
    return $this->answers;
  }

  /**
   * Add notifications
   *
   * @param \Ema\DomainBundle\Entity\Notification $notifications
   * @return Schedule
   */
  public function addNotification(\Ema\DomainBundle\Entity\Notification $notifications)
  {
    $this->notifications[] = $notifications;

    return $this;
  }

  /**
   * Remove notifications
   *
   * @param \Ema\DomainBundle\Entity\Notification $notifications
   */
  public function removeNotification(\Ema\DomainBundle\Entity\Notification $notifications)
  {
    $this->notifications->removeElement($notifications);
  }

  /**
   * Get notifications
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getNotifications()
  {
    return $this->notifications;
  }

  /**
   * Set user
   *
   * @param \Ema\DomainBundle\Entity\User $user
   * @return Schedule
   */
  public function setUser(\Ema\DomainBundle\Entity\User $user)
  {
    $this->user = $user;

    return $this;
  }

  /**
   * Get user
   *
   * @return \Ema\DomainBundle\Entity\User
   */
  public function getUser()
  {
    return $this->user;
  }
}
