<?php

namespace Ema\DomainBundle\Entity;


use Doctrine\ORM\EntityRepository;

class AnswerRepository extends EntityRepository {

  public function findTotalSubmittedAnswer(User $user, \DateTime $startTime, \DateTime $endTime) {
    $query = $this->createQueryBuilder('a')
        ->select('DISTINCT(a.submissionTime) as submissionTimes')
        ->where('a.user = :user')
        ->andWhere('a.submissionTime >= :startTime')
        ->andWhere('a.submissionTime <= :endTime')
        ->setParameter('user', $user)
        ->setParameter('startTime', $startTime)
        ->setParameter('endTime', $endTime)
        ->groupBy('a.submissionTime')
        ->getQuery();

    $result = $query->getResult();
    return count($result);
  }

  public function findTotalAnswerBetween($startDate, $endDate, $users) {
    $queryBuilder = $this->createQueryBuilder('a')
        ->select('count(a.id)')
        ->where('a.submissionTime between :startDate and :endDate')
        ->andWhere('a.user in (:users)')
        ->leftJoin('a.schedule', 's')
        ->leftJoin('a.notification', 'n')
        ->leftJoin('a.question', 'q')
        ->leftJoin('a.user', 'u')
        ->orderBy('a.user')
        ->addOrderBy('a.submissionTime')
        ->addOrderBy('a.question')
        ->setParameter('users', $users)
        ->setParameter('startDate', $startDate)
        ->setParameter('endDate', $endDate);

    $query = $queryBuilder->getQuery();
    return $query->getSingleScalarResult();
  }

  public function findAnswerBetween($startDate, $endDate, $users) {
    $queryBuilder = $this->createQueryBuilder('a')
        ->select('a')
        ->where('a.submissionTime between :startDate and :endDate')
        ->andWhere('a.user in (:users)')
        ->leftJoin('a.schedule', 's')
        ->leftJoin('a.notification', 'n')
        ->leftJoin('a.question', 'q')
        ->leftJoin('a.user', 'u')
        ->orderBy('u.username', 'asc')
        ->addOrderBy('a.submissionTime')
        ->addOrderBy('a.question')
        ->setParameter('users', $users)
        ->setParameter('startDate', $startDate)
        ->setParameter('endDate', $endDate);

    $query = $queryBuilder->getQuery();
    return $query->getResult();
  }

  public function getAllResult() {
    $query = $this->createQueryBuilder('a')
        ->select('a')
        ->leftJoin('a.schedule', 's')
        ->leftJoin('a.notification', 'n')
        ->leftJoin('a.question', 'q')
        ->leftJoin('a.user', 'u')
        ->orderBy('a.user')
        ->addOrderBy('a.submissionTime')
        ->addOrderBy('a.question')
        ->getQuery();

    return $query->getResult();
  }

}
