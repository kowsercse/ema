<?php

namespace Ema\DomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * User
 */
class User implements AdvancedUserInterface, \Serializable
{
  const SS_STUDY_GROUP = 'SS Study Group';
  const BONING_UP_GROUP = 'Boning Up Group';
  const PERSONAL_CHOICE_GROUP = 'Personal Choice Group';

  private static $roles = array(
      'admin' => 'ROLE_ADMIN',
      self::SS_STUDY_GROUP => 'ROLE_PARTICIPANT',
      self::BONING_UP_GROUP => 'ROLE_PARTICIPANT',
      self::PERSONAL_CHOICE_GROUP => 'ROLE_PARTICIPANT',
      'Volunteer' => 'ROLE_VOLUNTEER',
      'Super User' => 'ROLE_SUPER_USER',
      'Researcher Level 1' => 'ROLE_RESEARCHER',
      'Researcher Level 2' => 'ROLE_RESEARCHER',
  );

  /**
   * @var integer
   */
  private $id;

  /**
   * @var string
   */
  private $username;

  /**
   * @var string
   */
  private $email;

  /**
   * @var string
   */
  private $password;

  /**
   * @var string
   */
  private $role;

  /**
   * @var string
   */
  private $previousRole;

  /**
   * @var string
   */
  private $resetToken;

  /**
   * @var string
   */
  private $activationKey;

  /**
   * @var \DateTime
   */
  private $startDate;

  /**
   * @var \DateTime
   */
  private $lastLogin;

  /**
   * @var \Ema\DomainBundle\Entity\ContactingTime
   */
  private $contactingTime;

  /**
   * @var \Doctrine\Common\Collections\Collection
   */
  private $schedules;

  /**
   * @var boolean
   */
  private $active;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->schedules = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param string $username
   */
  public function setUsername($username)
  {
    $this->username = $username;
  }

  /**
   * @inheritdoc
   * @return string
   */
  public function getUsername()
  {
    return $this->username;
  }

  /**
   * Set email
   *
   * @param string $email
   * @return User
   */
  public function setEmail($email)
  {
    $this->email = $email;

    return $this;
  }

  /**
   * Get email
   *
   * @return string
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * Set password
   *
   * @param string $password
   * @return User
   */
  public function setPassword($password)
  {
    $this->password = $password;

    return $this;
  }

  /**
   * Get password
   *
   * @return string
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * Set role
   *
   * @param string $role
   * @return User
   */
  public function setRole($role)
  {
    $this->role = $role;

    return $this;
  }

  /**
   * Get role
   *
   * @return string
   */
  public function getRole()
  {
    return $this->role;
  }

  /**
   * @param string $resetToken
   */
  public function setResetToken($resetToken)
  {
    $this->resetToken = $resetToken;
  }

  /**
   * Get previousRole
   *
   * @return string
   */
  public function getPreviousRole() {
    return $this->previousRole;
  }

  /**
   * @param string $previousRole
   */
  public function setPreviousRole($previousRole) {
    $this->previousRole = $previousRole;
  }

  /**
   * @return string
   */
  public function getResetToken()
  {
    return $this->resetToken;
  }

  /**
   * @param string $activationKey
   */
  public function setActivationKey($activationKey)
  {
    $this->activationKey = $activationKey;
  }

  /**
   * @return string
   */
  public function getActivationKey()
  {
    return $this->activationKey;
  }

  /**
   * Set startDate
   *
   * @param \DateTime $startDate
   * @return User
   */
  public function setStartDate($startDate)
  {
    $this->startDate = $startDate;

    return $this;
  }

  /**
   * Get startDate
   *
   * @return \DateTime
   */
  public function getStartDate()
  {
    return $this->startDate;
  }

  /**
   * @return boolean
   */
  public function getActive() {
    return $this->active;
  }

  /**
   * @param boolean $active
   * @return $this
   */
  public function setActive($active) {
    $this->active = $active;

    return $this;
  }

  /**
   * @param \DateTime $lastLogin
   */
  public function setLastLogin($lastLogin)
  {
    $this->lastLogin = $lastLogin;
  }

  /**
   * @return \DateTime
   */
  public function getLastLogin()
  {
    return $this->lastLogin;
  }

  /**
   * Set contactingTime
   *
   * @param \Ema\DomainBundle\Entity\ContactingTime $contactingTime
   * @return User
   */
  public function setContactingTime(\Ema\DomainBundle\Entity\ContactingTime $contactingTime = null)
  {
    $this->contactingTime = $contactingTime;

    return $this;
  }

  /**
   * Get contactingTime
   *
   * @return \Ema\DomainBundle\Entity\ContactingTime
   */
  public function getContactingTime()
  {
    return $this->contactingTime;
  }

  /**
   * Add schedules
   *
   * @param \Ema\DomainBundle\Entity\Schedule $schedules
   * @return User
   */
  public function addSchedule(\Ema\DomainBundle\Entity\Schedule $schedules)
  {
    $this->schedules[] = $schedules;

    return $this;
  }

  /**
   * Remove schedules
   *
   * @param \Ema\DomainBundle\Entity\Schedule $schedules
   */
  public function removeSchedule(\Ema\DomainBundle\Entity\Schedule $schedules)
  {
    $this->schedules->removeElement($schedules);
  }

  /**
   * Get schedules
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getSchedules()
  {
    return $this->schedules;
  }

  function __toString()
  {
    return "" . $this->id;
  }

  /**
   * Returns the roles granted to the user.
   *
   * <code>
   * public function getRoles()
   * {
   *     return array('ROLE_USER');
   * }
   * </code>
   *
   * Alternatively, the roles might be stored on a ``roles`` property,
   * and populated in any number of different ways when the user object
   * is created.
   *
   * @return Role[] The user roles
   */
  public function getRoles()
  {
    return $this->role ? array(self::$roles[$this->role]) : array();
  }

  /**
   * Returns the salt that was originally used to encode the password.
   *
   * This can return null if the password was not encoded using a salt.
   *
   * @return string|null The salt
   */
  public function getSalt()
  {
    return null;
  }

  /**
   * Removes sensitive data from the user.
   *
   * This is important if, at any given point, sensitive information like
   * the plain-text password is stored on this object.
   */
  public function eraseCredentials()
  {
  }

  /**
   * @see \Serializable::serialize()
   */
  public function serialize()
  {
    return serialize(array(
        $this->id,
    ));
  }

  /**
   * @see \Serializable::unserialize()
   */
  public function unserialize($serialized)
  {
    list ($this->id,) = unserialize($serialized);
  }

  /**
   * Checks whether the user's account has expired.
   *
   * Internally, if this method returns false, the authentication system
   * will throw an AccountExpiredException and prevent login.
   *
   * @return Boolean true if the user's account is non expired, false otherwise
   *
   * @see AccountExpiredException
   */
  public function isAccountNonExpired() {
    return true;
  }

  /**
   * Checks whether the user is locked.
   *
   * Internally, if this method returns false, the authentication system
   * will throw a LockedException and prevent login.
   *
   * @return Boolean true if the user is not locked, false otherwise
   *
   * @see LockedException
   */
  public function isAccountNonLocked() {
    return true;
  }

  /**
   * Checks whether the user's credentials (password) has expired.
   *
   * Internally, if this method returns false, the authentication system
   * will throw a CredentialsExpiredException and prevent login.
   *
   * @return Boolean true if the user's credentials are non expired, false otherwise
   *
   * @see CredentialsExpiredException
   */
  public function isCredentialsNonExpired() {
    return true;
  }

  /**
   * Checks whether the user is enabled.
   *
   * Internally, if this method returns false, the authentication system
   * will throw a DisabledException and prevent login.
   *
   * @return Boolean true if the user is enabled, false otherwise
   *
   * @see DisabledException
   */
  public function isEnabled() {
    return $this->active;
  }
}
