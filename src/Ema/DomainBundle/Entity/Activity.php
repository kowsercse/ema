<?php

namespace Ema\DomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activity
 */
class Activity
{
  /**
   * @var integer
   */
  private $id;

  /**
   * @var string
   */
  private $uniqueName;

  /**
   * @var string
   */
  private $title;

  /**
   * @var \Doctrine\Common\Collections\Collection
   */
  private $questions;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set uniqueName
   *
   * @param string $uniqueName
   * @return Activity
   */
  public function setUniqueName($uniqueName)
  {
    $this->uniqueName = $uniqueName;

    return $this;
  }

  /**
   * Get uniqueName
   *
   * @return string
   */
  public function getUniqueName()
  {
    return $this->uniqueName;
  }

  /**
   * Set title
   *
   * @param string $title
   * @return Activity
   */
  public function setTitle($title)
  {
    $this->title = $title;

    return $this;
  }

  /**
   * Get title
   *
   * @return string
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * Add questions
   *
   * @param \Ema\DomainBundle\Entity\Question $questions
   * @return Activity
   */
  public function addQuestion(\Ema\DomainBundle\Entity\Question $questions)
  {
    $this->questions[] = $questions;

    return $this;
  }

  /**
   * Remove questions
   *
   * @param \Ema\DomainBundle\Entity\Question $questions
   */
  public function removeQuestion(\Ema\DomainBundle\Entity\Question $questions)
  {
    $this->questions->removeElement($questions);
  }

  /**
   * Get questions
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getQuestions()
  {
    return $this->questions;
  }
}
