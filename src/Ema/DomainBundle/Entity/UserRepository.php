<?php
/**
 * Created by PhpStorm.
 * User: kowser
 * Date: 3/11/14
 * Time: 1:44 AM
 */

namespace Ema\DomainBundle\Entity;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserRepository extends EntityRepository implements UserProviderInterface {

  public function getUsersRequireSchedule()
  {
    $query = $this->createQueryBuilder('u')
        ->leftJoin('u.schedules', 's')
        ->where('s.id is null')
        ->andWhere("u.role in ('SS Study Group', 'Boning Up Group', 'Personal Choice Group')")
        ->getQuery();

    return $query->getResult();
  }

  /**
   * @inheritdoc
   */
  public function loadUserByUsername($username)
  {
    $query = $this->createQueryBuilder('u')
        ->where('u.username = :username')
        ->setParameter('username', $username)
        ->getQuery();

    try {
      /* @var User $user */
      $user = $query->getSingleResult();
      if(!$user->getActive()) {
        throw new UsernameNotFoundException("The user is not active studyId: " . $user->getUsername());
      }
    } catch (NoResultException $e) {
      $message = sprintf(
          'Unable to find an active user EmaDomainBundle:User object identified by "%s".',
          $username
      );
      throw new UsernameNotFoundException($message, 0, $e);
    }

    return $user;
  }

  /**
   * @inheritdoc
   */
  public function refreshUser(UserInterface $user)
  {
    $class = get_class($user);
    if (!$this->supportsClass($class)) {
      throw new UnsupportedUserException(
          sprintf(
              'Instances of "%s" are not supported.',
              $class
          )
      );
    }

    return $this->find($user->getId());
  }

  /**
   * @inheritdoc
   */
  public function supportsClass($class)
  {
    return $this->getEntityName() === $class || is_subclass_of($class, $this->getEntityName());
  }

  public function findByUsername($username) {
    $query = $this->createQueryBuilder('u')
        ->where('u.username = :username')
        ->setParameter('username', $username)
        ->getQuery();

    return $query->getOneOrNullResult();
  }

  /**
   * @param User $user
   * @return array|null
   * @throws \Doctrine\DBAL\DBALException
   */
  public function getBaselineRow(User $user) {
    $query = "SELECT * FROM baseline b WHERE b.study_id = '".$user->getUsername()."'";
    $statement = $this->getEntityManager()->getConnection()->executeQuery($query);
    while($baseline = $statement->fetch()) {
      return $baseline;
    }

    return null;
  }

  public function findUsersByRole($role, $limit = 10000, $offset = 0) {
    $query = $this->createQueryBuilder('u')
        ->where('u.role = :role')
        ->setParameter('role', $role)
        ->setMaxResults($limit)
        ->setFirstResult($offset)
        ->orderBy('u.username')
        ->getQuery();

    return $query->getResult();
  }

}
