<?php

namespace Ema\DomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Notification
{
  /**
   * @var \DateTime
   */
  private $scheduledTime;

  /**
   * @var boolean
   */
  private $sent;

  /**
   * @var string
   */
  private $status;

  /**
   * @var integer
   */
  private $serial;

  /**
   * @var integer
   */
  private $id;

  /**
   * @var \Doctrine\Common\Collections\Collection
   */
  private $answers;

  /**
   * @var \Ema\DomainBundle\Entity\Schedule
   */
  private $schedule;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Set scheduledTime
   *
   * @param \DateTime $scheduledTime
   * @return Notification
   */
  public function setScheduledTime($scheduledTime)
  {
    $this->scheduledTime = $scheduledTime;

    return $this;
  }

  /**
   * Get scheduledTime
   *
   * @return \DateTime
   */
  public function getScheduledTime()
  {
    return $this->scheduledTime;
  }

  /**
   * Set sent
   *
   * @param boolean $sent
   * @return Notification
   */
  public function setSent($sent)
  {
    $this->sent = $sent;

    return $this;
  }

  /**
   * Get sent
   *
   * @return boolean
   */
  public function getSent()
  {
    return $this->sent;
  }

  /**
   * Set status
   *
   * @param string $status
   * @return Notification
   */
  public function setStatus($status)
  {
    $this->status = $status;

    return $this;
  }

  /**
   * Get status
   *
   * @return string
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Set serial
   *
   * @param integer $serial
   * @return Notification
   */
  public function setSerial($serial)
  {
    $this->serial = $serial;

    return $this;
  }

  /**
   * Get serial
   *
   * @return integer
   */
  public function getSerial()
  {
    return $this->serial;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set schedule
   *
   * @param \Ema\DomainBundle\Entity\Schedule $schedule
   * @return Notification
   */
  public function setSchedule(\Ema\DomainBundle\Entity\Schedule $schedule)
  {
    $this->schedule = $schedule;

    return $this;
  }

  /**
   * Get schedule
   *
   * @return \Ema\DomainBundle\Entity\Schedule
   */
  public function getSchedule()
  {
    return $this->schedule;
  }

  /**
   * Add answers
   *
   * @param \Ema\DomainBundle\Entity\Answer $answers
   * @return Notification
   */
  public function addAnswer(\Ema\DomainBundle\Entity\Answer $answers)
  {
    $this->answers[] = $answers;

    return $this;
  }

  /**
   * Remove answers
   *
   * @param \Ema\DomainBundle\Entity\Answer $answers
   */
  public function removeAnswer(\Ema\DomainBundle\Entity\Answer $answers)
  {
    $this->answers->removeElement($answers);
  }

  /**
   * Get answers
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getAnswers()
  {
    return $this->answers;
  }

  function __toString()
  {
    return "Notification[id=" . $this->id . ", scheduledTime=" . $this->scheduledTime->format('Y-m-d H:i:s') . "]";
  }

}
