<?php

namespace Ema\DomainBundle\Entity;

use Doctrine\ORM\EntityRepository;


class ScheduleRepository extends EntityRepository
{
  public function findTotalScheduleBy(User $user) {
    $query = $this->createQueryBuilder('s')
        ->select('COUNT(s.id) as total')
        ->where('s.user = :user')
        ->setParameter('user', $user)
        ->getQuery();

    $result = $query->getSingleResult();
    return $result['total'];
  }

  public function findTotalDeniedScheduleBy(User $user, \DateTime $startDay, \DateTime $endDay) {
    $query = $this->createQueryBuilder('s')
        ->select('COUNT(s.id) as total')
        ->where('s.user = :user')
        ->andWhere('s.surveyDate >= :startDay')
        ->andWhere('s.surveyDate <= :endDay')
        ->andWhere('s.denied = :denied')
        ->setParameter('denied', true)
        ->setParameter('user', $user)
        ->setParameter('startDay', $startDay)
        ->setParameter('endDay', $endDay)
        ->getQuery();

    $result = $query->getSingleResult();
    return $result['total'];
  }

  /**
   * @param \DateTime|string $startTime
   * @param \DateTime|string $endTime
   * @param null $studyId
   * @return array
   */
  public function findSchedulesBetween($startTime, $endTime, $studyId = null) {
    $queryBuilder = $this->createQueryBuilder('s')
        ->leftJoin('s.user', 'u')
        ->leftJoin('s.notifications', 'n')
        ->leftJoin('n.answers', 'a')
        ->where('s.surveyDate between :startDate and :endDate')
        ->orderBy('u.username, s.id', 'asc')
        ->setParameter('startDate', $startTime)
        ->setParameter('endDate', $endTime);

    if($studyId) {
      $queryBuilder->andWhere('u.username = :studyId');
      $queryBuilder->setParameter('studyId', $studyId);
    }

    $query = $queryBuilder->getQuery();
    return $query->getResult();
  }

}
