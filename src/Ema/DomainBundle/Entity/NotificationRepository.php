<?php

namespace Ema\DomainBundle\Entity;


use Doctrine\ORM\EntityRepository;

class NotificationRepository extends EntityRepository {

  public function findNotificationAt(\DateTime $scheduledTime, User $user) {
    $query = $this->createQueryBuilder('n')
        ->leftJoin('n.schedule', 's')
        ->andWhere('n.scheduledTime = :scheduledTime')
        ->andWhere('s.user = :user')
        ->setParameter('scheduledTime', $scheduledTime)
        ->setParameter('user', $user)
        ->getQuery();

    return $query->getOneOrNullResult();
  }

}
