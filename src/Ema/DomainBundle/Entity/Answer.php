<?php

namespace Ema\DomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Answer
 */
class Answer
{

  /**
   * @var integer
   */
  private $answer;

  /**
   * @var \DateTime
   */
  private $submissionTime;

  /**
   * @var integer
   */
  private $id;

  /**
   * @var \Ema\DomainBundle\Entity\Notification
   */
  private $notification;

  /**
   * @var \Ema\DomainBundle\Entity\User
   */
  private $user;

  /**
   * @var \Ema\DomainBundle\Entity\Question
   */
  private $question;

  /**
   * @var \Ema\DomainBundle\Entity\Schedule
   */
  private $schedule;


  /**
   * Set answer
   *
   * @param integer $answer
   * @return Answer
   */
  public function setAnswer($answer)
  {
    $this->answer = $answer;

    return $this;
  }

  /**
   * Get answer
   *
   * @return integer
   */
  public function getAnswer()
  {
    return $this->answer;
  }

  /**
   * Set submissionTime
   *
   * @param \DateTime $submissionTime
   * @return Answer
   */
  public function setSubmissionTime($submissionTime)
  {
    $this->submissionTime = $submissionTime;

    return $this;
  }

  /**
   * Get submissionTime
   *
   * @return \DateTime
   */
  public function getSubmissionTime()
  {
    return $this->submissionTime;
  }

  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set notification
   *
   * @param \Ema\DomainBundle\Entity\Notification $notification
   * @return Answer
   */
  public function setNotification(\Ema\DomainBundle\Entity\Notification $notification = null)
  {
    $this->notification = $notification;

    return $this;
  }

  /**
   * Get notification
   *
   * @return \Ema\DomainBundle\Entity\Notification
   */
  public function getNotification()
  {
    return $this->notification;
  }

  /**
   * Set user
   *
   * @param \Ema\DomainBundle\Entity\User $user
   * @return Answer
   */
  public function setUser(\Ema\DomainBundle\Entity\User $user)
  {
    $this->user = $user;

    return $this;
  }

  /**
   * Get user
   *
   * @return \Ema\DomainBundle\Entity\User
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * Set question
   *
   * @param \Ema\DomainBundle\Entity\Question $question
   * @return Answer
   */
  public function setQuestion(\Ema\DomainBundle\Entity\Question $question)
  {
    $this->question = $question;

    return $this;
  }

  /**
   * Get question
   *
   * @return \Ema\DomainBundle\Entity\Question
   */
  public function getQuestion()
  {
    return $this->question;
  }

  /**
   * Set schedule
   *
   * @param \Ema\DomainBundle\Entity\Schedule $schedule
   * @return Answer
   */
  public function setSchedule(\Ema\DomainBundle\Entity\Schedule $schedule = null)
  {
    $this->schedule = $schedule;

    return $this;
  }

  /**
   * Get schedule
   *
   * @return \Ema\DomainBundle\Entity\Schedule
   */
  public function getSchedule()
  {
    return $this->schedule;
  }
}
