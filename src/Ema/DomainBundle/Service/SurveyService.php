<?php

namespace Ema\DomainBundle\Service;


use Doctrine\ORM\EntityManager;
use Ema\DomainBundle\Entity\Notification;
use Ema\DomainBundle\Entity\NotificationRepository;
use Ema\DomainBundle\Entity\User;

class SurveyService
{
  /**
   * @var EntityManager
   */
  private $entityManager;

  function __construct(EntityManager $entityManager)
  {
    $this->entityManager = $entityManager;
  }

  /**
   * @param User $user
   * @return Notification
   */
  public function getCurrentNotification(User $user)
  {
    $now = new \DateTime();
    $now->setTime($now->format('H'), $this->calculateMinute($now), 0);

    /**
     * @var NotificationRepository $repository
     */
    $repository = $this->entityManager->getRepository('EmaDomainBundle:Notification');
    return $repository->findNotificationAt($now, $user);
  }

  /**
   * @param \DateTime $now
   * @return string
   */
  protected function calculateMinute(\DateTime $now)
  {
    $minute = $now->format("i");
    if($minute < 15) {
      return 0;
    }
    else if($minute < 30) {
      return 15;
    }
    else if($minute < 45) {
      return 30;
    }
    else {
      return 45;
    }
  }

}
