<?php

namespace Ema\DomainBundle\Service;


use Doctrine\ORM\EntityManager;
use Ema\DomainBundle\Entity\ContactingTime;
use Ema\DomainBundle\Entity\User;

class ContactingTimeService {
  /**
   * @var EntityManager
   */
  private $entityManager;

  private $defaultStartTime;

  function __construct(EntityManager $entityManager, $startTime)
  {
    $this->entityManager = $entityManager;
    $this->defaultStartTime = new \DateTime($startTime);
  }

  public function loadContactingTimeFor(User $user) {
    $contactingTime = $user->getContactingTime();
    if($contactingTime == null) {
      $contactingTime = new ContactingTime();
      $contactingTime->setUser($user);
      $contactingTime->setStartTime($this->defaultStartTime);

      $this->entityManager->persist($contactingTime);
      $this->entityManager->flush();
    }

    return $contactingTime;
  }

}
