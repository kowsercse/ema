<?php

namespace Ema\DomainBundle\Service;


use Doctrine\ORM\EntityManager;
use Ema\DomainBundle\Entity\AnswerRepository;
use Ema\DomainBundle\Entity\Schedule;
use Ema\DomainBundle\Entity\ScheduleRepository;
use Ema\DomainBundle\Entity\User;

class ScheduleGeneratorService
{

  const WEEK_IN_YEAR = 52;
  const DAY_IN_WEEK = 7;
  const WEEK_PER_ROUND = 4;
  const DAY_PER_ROUND = 28;

  /**
   * @var EntityManager
   */
  private $entityManager;

  /**
   * @var string
   */
  private $specialGroup;

  function __construct(EntityManager $entityManager, $specialGroup)
  {
    $this->entityManager = $entityManager;
    $this->specialGroup = $specialGroup;
  }

  public function findTotalScheduleBy(User $user)
  {
    return $this->entityManager->getRepository("EmaDomainBundle:Schedule")->findTotalScheduleBy($user);
  }

  public function generateSchedule(User $user)
  {
    /**
     * @var Schedule $schedule
     */
    if ($this->findTotalScheduleBy($user)) {
      throw new \InvalidArgumentException("Schedule are already generated for user: " . $user->getId());
    }

    $total = 0;
    $startDate = clone $user->getStartDate();
    $startDate->setTime(0, 0, 0);
    $schedules = array();
    for ($i = 1; $i <= 13; $i++) {
      if ($this->specialGroup == $user->getRole()) {
        $generatedSchedule = $this->generateScheduleForRoundForSpecificGroup($startDate, $i);
      } else {
        $generatedSchedule = $this->generateScheduleForRound($startDate, $i);
      }
      $count = count($generatedSchedule);
      $total += $count;
      foreach($generatedSchedule as $schedule) {
        echo "<br>" . $schedule->getSurveyDate()->format("Y-m-d");
      }
      echo "<br/> Round: $i count: " . $count . " total: " . $total . "<br/>";
      $schedules = array_merge($schedules, $generatedSchedule);
    }

    foreach ($schedules as $schedule) {
      $schedule->setUser($user);
    }

    foreach ($schedules as $schedule) {
      $this->entityManager->persist($schedule);
    }
    $this->entityManager->flush();
  }

  /**
   * @param \DateTime $startDate
   * @param $round
   * @return array
   * @throws \InvalidArgumentException
   */
  private function generateScheduleForRound(\DateTime $startDate, $round)
  {
    if ($round < 1 && $round > 13) {
      throw new \InvalidArgumentException("Round should be withing 1 to 13, found " . $round);
    }

    if ($round == 1) {
      return $this->generateDailySchedules($startDate, 0, 4);
    } else if ($round == 2) {
      return $this->generateDailySchedules($startDate, 1, 2);
    } else if ($round == 3) {
      return $this->generateDailySchedules($startDate, 2, 1);
    } else if ($round == 4) {
      return $this->generateDailySchedules($startDate, 3, 1);
    } else if ($round == 5) {
      return $this->generateIntervalSchedules($startDate, 4, 2);
    } else if ($round == 6) {
      return $this->generateIntervalSchedules($startDate, 5, 3);
    } else if ($round == 7) {
      return $this->generateWeeklyDoubleRandomSchedules($startDate, 6);
    } else {
      return $this->generateWeeklyRandomSchedules($startDate, $round - 1);
    }
  }

  /**
   * @param \DateTime $startDate
   * @param $round
   * @return array
   * @throws \InvalidArgumentException
   */
  private function generateScheduleForRoundForSpecificGroup(\DateTime $startDate, $round)
  {
    if ($round < 1 && $round > 13) {
      throw new \InvalidArgumentException("Round should be withing 1 to 13, found " . $round);
    }

    if ($round == 1) {
      return $this->generateDailySchedules($startDate, 0, 1);
    } else if ($round == 2) {
      return $this->generateIntervalSchedules($startDate, 1, 2);
    } else if ($round == 3) {
      return $this->generateIntervalSchedules($startDate, 2, 3);
    } else if ($round == 4) {
      return $this->generateWeeklyDoubleRandomSchedules($startDate, 3);
    } else if ($round >= 5 && $round <= 10) {
      return $this->generateWeeklyRandomSchedules($startDate, $round - 1);
    } else if ($round == 11) {
      return $this->generateDailySchedules($startDate, 10, 4);
    } else if ($round == 12) {
      return $this->generateDailySchedules($startDate, 11, 2);
    } else if ($round == 13) {
      return $this->generateDailySchedules($startDate, 12, 1);
    }
  }

  private function generateWeeklyDoubleRandomSchedules(\DateTime $startDate, $round)
  {
    $start = 1 + self::DAY_IN_WEEK * self::WEEK_PER_ROUND * $round;
    $end = self::DAY_PER_ROUND + self::DAY_IN_WEEK * self::WEEK_PER_ROUND * $round;

    $schedules = array();
    for ($i = $start; $i <= $end; $i += 7) {
      $schedule = $this->generateRandomSchedule($startDate, 0, 2, $i);
      $schedules[] = $schedule;

      $schedule = $this->generateRandomSchedule($startDate, 4, 6, $i);
      $schedules[] = $schedule;
    }
    return $schedules;
  }

  private function generateWeeklyRandomSchedules(\DateTime $startDate, $round)
  {
    $start = 1 + self::DAY_IN_WEEK * self::WEEK_PER_ROUND * $round;
    $end = self::DAY_PER_ROUND + self::DAY_IN_WEEK * self::WEEK_PER_ROUND * $round;

    $schedules = array();
    for ($i = $start; $i <= $end; $i += 7) {
      $schedule = $this->generateRandomSchedule($startDate, 0, 6, $i);
      $schedules[] = $schedule;
    }
    return $schedules;
  }

  private function generateIntervalSchedules(\DateTime $startDate, $round, $interval)
  {
    $start = 1 + self::DAY_IN_WEEK * self::WEEK_PER_ROUND * $round;
    $end = self::DAY_PER_ROUND + self::DAY_IN_WEEK * self::WEEK_PER_ROUND * $round;

    $schedules = array();
    for ($i = $start; $i <= $end; $i++) {
      if ($i % $interval == 0) {
        $notificationDate = clone $startDate;
        $notificationDate->setTime(0, 0, 0);
        $notificationDate->modify("+$i day");

        $schedule = $this->createEmptySchedule()->setSurveyDate($notificationDate);
        $schedules[] = $schedule;
      }
    }
    return $schedules;
  }

  /**
   * @param \DateTime $startDate
   * @param $round
   * @param $timesPerDay
   * @return array
   */
  private function generateDailySchedules(\DateTime $startDate, $round, $timesPerDay)
  {
    $start = 1 + self::DAY_IN_WEEK * self::WEEK_PER_ROUND * $round;
    $end = self::DAY_PER_ROUND + self::DAY_IN_WEEK * self::WEEK_PER_ROUND * $round;

    $schedules = array();
    for ($i = $start; $i <= $end; $i++) {
      for ($j = 0; $j < $timesPerDay; $j++) {
        $notificationDate = clone $startDate;
        $notificationDate->modify("+$i day");

        $schedule = $this->createEmptySchedule()->setSurveyDate($notificationDate);
        $schedules[] = $schedule;
      }
    }
    return $schedules;
  }

  /**
   * @return Schedule
   */
  private function createEmptySchedule()
  {
    $schedule = new Schedule();
    $schedule->setDenied(false);
    return $schedule;
  }

  /**
   * @param \DateTime $startDate
   * @param $min
   * @param $max
   * @param $i
   * @return Schedule
   */
  private function generateRandomSchedule(\DateTime $startDate, $min, $max, $i)
  {
    $random = rand($min, $max) + $i;
    $week = ($i - 1) /7 + 1;
    echo "<br > $week($i) -> [$random]";
    $notificationDate = clone $startDate;
    $notificationDate->setTime(0, 0, 0);
    $notificationDate->modify("+$random day");

    $schedule = $this->createEmptySchedule()->setSurveyDate($notificationDate);
    return $schedule;
  }


  public function getProgress(User $user)
  {
    /**
     * @var ScheduleRepository $scheduleRepository
     * @var AnswerRepository $answerRepository
     */
    $totalSchedule = $this->getTotalScheduleBetween($user);
    echo $totalSchedule;

    $totalSubmitted = $this->getTotalSubmitted($user);
    $totalDenied = $this->getTotalDenied($user);

    $totalSchedule = (0.75) * $totalSchedule;
    $progress = ($totalSubmitted + $totalDenied) * 100 / $totalSchedule;
    return $progress > 100 ? 100 : $progress;
  }

  /**
   * @param User $user
   * @return \DateTime
   */
  private function getStartDate(User $user)
  {
    $startDate = clone $user->getStartDate();
    $startDate->setTime(0, 0, 0);

    if ($this->specialGroup != $user->getRole()) {
      $firstGiftDay = self::DAY_PER_ROUND * 3 + 1;
    } else {
      $firstGiftDay = self::DAY_PER_ROUND * 9 + 1;
    }

    $dateInterval = $this->getToday()->diff($startDate);
    if ($dateInterval->days >= $firstGiftDay) {
      $startDate->modify("+$firstGiftDay day");
    }

    return $startDate;
  }

  /**
   * @return \DateTime
   */
  private function getToday()
  {
    $today = new \DateTime();
    $today->setTime(0, 0, 0);
    $today->modify("+1 day");
    return $today;
  }

  /**
   * @param User $user
   * @return int
   */
  public function getTotalScheduleBetween(User $user)
  {
    $startDate = clone $user->getStartDate();
    $startDate->setTime(0, 0, 0);

    if ($this->specialGroup != $user->getPreviousRole()) {
      $firstGiftDay = self::DAY_PER_ROUND * 3 + 1;
      $dateInterval = $this->getToday()->diff($startDate);
      return $dateInterval->days < $firstGiftDay ? 196 : 84;
    } else {
      $firstGiftDay = self::DAY_PER_ROUND * 9 + 1;
      $dateInterval = $this->getToday()->diff($startDate);
      return $dateInterval->days < $firstGiftDay ? 84 : 196;
    }
  }

  /**
   * @param User $user
   * @return int
   */
  public function getTotalSubmitted(User $user) {
    $startDate = $this->getStartDate($user);
    $today = $this->getToday();

    $totalSubmitted = $this->entityManager->getRepository('EmaDomainBundle:Answer')->findTotalSubmittedAnswer($user, $startDate, $today);
    return $totalSubmitted;
  }

  /**
   * @param User $user
   * @return int
   */
  public function getTotalDenied(User $user) {
    $startDate = $this->getStartDate($user);
    $today = $this->getToday();
    $totalDenied = $this->entityManager->getRepository('EmaDomainBundle:Schedule')->findTotalDeniedScheduleBy($user, $startDate, $today);
    return $totalDenied;
  }

}
