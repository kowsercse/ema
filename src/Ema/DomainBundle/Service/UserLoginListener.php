<?php

namespace Ema\DomainBundle\Service;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;

class UserLoginListener
{
  /**
   * @var EntityManager
   */
  private $entityManager;

  function __construct(EntityManager $entityManager)
  {
    $this->entityManager = $entityManager;
  }

  public function onLogin(AuthenticationEvent $event)
  {
    $user = $event->getAuthenticationToken()->getUser();
    $now = new \DateTime();
    $now->modify("-1 day");
    if($now > $user->getLastLogin()) {
      $user->setLastLogin(new \DateTime());
      $this->entityManager->flush();
    }
  }

}
