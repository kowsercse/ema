<?php

namespace Ema\SurveyBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ProfileController extends Controller
{

  public function resetLoginAction($id, $token)
  {
    $em = $this->getDoctrine()->getManager();
    $user = $em->getRepository('EmaDomainBundle:User')->find($id);
    if ($user && $user->getResetToken() === $token) {
      $user->setResetToken("");
      $user->setLastLogin(new \DateTime());
      $em->flush();
      return $this->render('EmaSurveyBundle:Profile:resetLogin.html.twig');
    }

    throw $this->createNotFoundException("Not Found");
  }

}
