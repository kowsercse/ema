INSERT INTO `Activity` (`id`, `uniqueName`, `title`) VALUES ('1', 'calcium', 'Calcium');
INSERT INTO `Activity` (`id`, `uniqueName`, `title`) VALUES ('2', 'balance', 'Balance');
INSERT INTO `Activity` (`id`, `uniqueName`, `title`) VALUES ('3', 'strength', 'Strength');
INSERT INTO `Activity` (`id`, `uniqueName`, `title`) VALUES ('4', 'physicalActivity', 'Physical Activity');

INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ( '1', '1', 'I did something to increase my calcium intake');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ( '2', '1', 'I had a specific goal');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ( '3', '1', 'I had specific plans');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ( '4', '1', 'I tracked what I did');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ( '5', '1', 'I thought about the reasons why or why not my plans worked');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ( '6', '1', 'I made a decision to modify or change my goal');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ( '7', '1', 'My feelings affected my behavior');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ( '8', '1', 'I really wanted to meet my goal, but I took care of other things instead');

INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ( '9', '2', 'I did something to increase my balance');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('10', '2', 'I had a specific goal');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('11', '2', 'I had specific plans');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('12', '2', 'I tracked what I did');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('13', '2', 'I thought about the reasons why or why not my plans worked');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('14', '2', 'I made a decision to modify or change my goal');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('15', '2', 'My feelings affected my behavior');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('16', '2', 'I really wanted to meet my goal, but I took care of other things instead');

INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('17', '3', 'I did something to increase my strength');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('18', '3', 'I had a specific goal');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('19', '3', 'I had specific plans');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('20', '3', 'I tracked what I did');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('21', '3', 'I thought about the reasons why or why not my plans worked');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('22', '3', 'I made a decision to modify or change my goal');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('23', '3', 'My feelings affected my behavior');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('24', '3', 'I really wanted to meet my goal, but I took care of other things instead');

INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('25', '4', 'I did something to increase my physical activity');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('26', '4', 'I had a specific goal');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('27', '4', 'I had specific plans');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('28', '4', 'I tracked what I did');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('29', '4', 'I thought about the reasons why or why not my plans worked');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('30', '4', 'I made a decision to modify or change my goal');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('31', '4', 'My feelings affected my behavior');
INSERT INTO `EmaQuestion` (`id`, `activityId`, `title`) VALUES ('32', '4', 'I really wanted to meet my goal, but I took care of other things instead');
