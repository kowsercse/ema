ALTER TABLE `user` ADD UNIQUE INDEX `name_UNIQUE` (`name` ASC);
ALTER TABLE `user` CHANGE COLUMN `name` `name` VARCHAR(64) NOT NULL;


ALTER TABLE `Activity_Assessment` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `Assessment_Count` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `Balance_Test_Result` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `BU_Tracking` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `Calcium_Intake` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `Confidence` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `Exercise_Level` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `Fine_Test_Result` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `Fine_Test_Over_Time` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `Goal_Congruence` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `Goal_Track` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `Strength_Test_Result` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `Tracking` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `User_Diet` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `User_Menopausal` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `baseline` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `enrolled_user` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `rogue_participant` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `wait_list` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `completion` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;
ALTER TABLE `user_answers` CHANGE COLUMN `study_id` `study_id` VARCHAR(64) NOT NULL;


ALTER TABLE `Activity_Assessment` ADD CONSTRAINT `FK_Activity_Assessment_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `Assessment_Count` ADD CONSTRAINT `FK_Assessment_Count_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `Balance_Test_Result` ADD CONSTRAINT `FK_Balance_Test_Result_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `BU_Tracking` ADD CONSTRAINT `FK_BU_Tracking_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `Calcium_Intake` ADD CONSTRAINT `FK_Calcium_Intake_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `Confidence` ADD CONSTRAINT `FK_Confidence_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `Exercise_Level` ADD CONSTRAINT `FK_Exercise_Level_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `Fine_Test_Result` ADD CONSTRAINT `FK_Fine_Test_Result_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `Fine_Test_Over_Time` ADD CONSTRAINT `FK_Fine_Test_Over_Time_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `Goal_Congruence` ADD CONSTRAINT `FK_Goal_Congruence_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `Goal_Track` ADD CONSTRAINT `FK_Goal_Track_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `Strength_Test_Result` ADD CONSTRAINT `FK_Strength_Test_Result_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `Tracking` ADD CONSTRAINT `FK_Tracking_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `User_Diet` ADD CONSTRAINT `FK_User_Diet_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `User_Menopausal` ADD CONSTRAINT `FK_User_Menopausal_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `baseline` ADD CONSTRAINT `FK_baseline_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
-- ALTER TABLE `enrolled_user` ADD CONSTRAINT `FK_enrolled_user_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `rogue_participant` ADD CONSTRAINT `FK_rogue_participant_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
ALTER TABLE `wait_list` ADD CONSTRAINT `FK_wait_list_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
-- ALTER TABLE `completion` ADD CONSTRAINT `FK_completion_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
-- ALTER TABLE `user_answers` ADD CONSTRAINT `FK_user_answers_User` FOREIGN KEY (`study_id`) REFERENCES `user` (`name`);
