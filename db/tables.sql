-- -----------------------------------------------------
-- Table `Activity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Activity` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'The id to uniquely identify each item',
  `uniqueName` VARCHAR(50)CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The unique name for the activity',
  `title` VARCHAR(500) NOT NULL COMMENT 'The activity name',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UNIQUE_Activity_uniqueName` (`uniqueName` ASC)
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `EmaQuestion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EmaQuestion` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'The id to uniquely identify each item',
  `title` VARCHAR(500)CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The question',
  `activityId` INT(11) NOT NULL COMMENT 'The ID of activity',
  PRIMARY KEY (`id`),
  INDEX `IDX_Question_activityId` (`activityId` ASC),
  CONSTRAINT `FK_Question_Activity` FOREIGN KEY (`activityId`)
  REFERENCES `Activity` (`id`)
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `EmaSchedule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EmaSchedule` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'The id to uniquely identify each item',
  `userId` INT(11) NOT NULL COMMENT 'The ID of user',
  `surveyDate` DATE NOT NULL COMMENT 'The schedule date on which EMA will be sent',
  `denied` TINYINT(1) NOT NULL COMMENT 'If the user have chosen not to answer the EMA',
  PRIMARY KEY (`id`),
  INDEX `IDX_Schedule_userId` (`userId` ASC),
  CONSTRAINT `FK_Schedule_User` FOREIGN KEY (`userId`)
  REFERENCES `user` (`id`)
    ON DELETE RESTRICT ON UPDATE RESTRICT
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `Notification`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Notification` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'The id to uniquely identify each item',
  `scheduleId` INT(11) NOT NULL COMMENT 'The ID of schedule',
  `scheduledTime` DATETIME NOT NULL COMMENT 'The time when the notification is sent',
  `sent` TINYINT(1) NOT NULL COMMENT 'Not needed',
  `serial` INT(11) NOT NULL COMMENT 'Store whether the notification is 1st, 2nd or 3rd.',
  `status` TEXT CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'Not needed',
  PRIMARY KEY (`id`),
  INDEX `IDX_Notification_Schedule` (`scheduleId` ASC),
  CONSTRAINT `FK_Notification_Schedule` FOREIGN KEY (`scheduleId`)
  REFERENCES `EmaSchedule` (`id`)
    ON DELETE RESTRICT ON UPDATE RESTRICT
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `EmaAnswer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EmaAnswer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'The id to uniquely identify each item',
  `userId` INT(11) NOT NULL COMMENT 'The id of the user',
  `scheduleId` INT(11) NULL DEFAULT NULL COMMENT 'The schedule id',
  `notificationId` INT(11) NULL DEFAULT NULL COMMENT 'The notification id',
  `questionId` INT(11) NOT NULL COMMENT 'The question id',
  `answer` INT(11) UNSIGNED NOT NULL COMMENT 'The answer for the question. 1=Yes, 0=No.',
  `submissionTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The time when this answer is submitted',
  PRIMARY KEY (`id`),
  INDEX `IDX_Answer_questionId` (`questionId` ASC),
  INDEX `IDX_Answer_userId` (`userId` ASC),
  INDEX `IDX_Answer_scheduleId` (`scheduleId` ASC),
  INDEX `IDX_Answer_notificationId` (`notificationId` ASC),
  KEY `IDX_Answer_submissionTime` (`submissionTime`),
  KEY `UNQ_Answer` (`userId`,`submissionTime`,`questionId`),
  CONSTRAINT `FK_Answer_Question` FOREIGN KEY (`questionId`)
  REFERENCES `EmaQuestion` (`id`)
    ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Answer_User` FOREIGN KEY (`userId`)
  REFERENCES `user` (`id`)
    ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Answer_Schedule` FOREIGN KEY (`scheduleId`)
  REFERENCES `EmaSchedule` (`id`)
    ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Answer_Notification` FOREIGN KEY (`notificationId`)
  REFERENCES `Notification` (`id`)
    ON DELETE RESTRICT ON UPDATE RESTRICT
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ContactingTime`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ContactingTime` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'The id to uniquely identify each item',
  `startTime` TIME NOT NULL COMMENT 'The start time, since when EMA can be sent',
  `userId` INT(11) NOT NULL COMMENT 'The id of the user',
  PRIMARY KEY (`id`),
  INDEX `IDX_ContactingTime_userId` (`userId` ASC),
  UNIQUE INDEX `UNIQUE_ContactingTime_userId` (`userId` ASC),
  CONSTRAINT `FK_ContactingTime_User` FOREIGN KEY (`userId`)
  REFERENCES `user` (`id`)
    ON DELETE RESTRICT ON UPDATE RESTRICT
)  ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE = utf8_bin;


CREATE TABLE IF NOT EXISTS `Message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The id to uniquely identify each item',
  `content` text COLLATE utf8_bin NOT NULL COMMENT 'The message which are sent randomly',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
