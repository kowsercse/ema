DROP VIEW IF EXISTS EmaData;
CREATE VIEW EmaData AS
    SELECT
        a.id as answerId,
        a.answer,
        a.questionId,
        a.submissionTime,
        a.userId,
        a.scheduleId,
        a.notificationId,
        s.surveyDate,
        s.denied,
        q.title as questionTitle,
        q.activityId,
        ac.title as activityTitle,
        ac.uniqueName,
        n.scheduledTime,
        n.serial,
        u.name as study_id,
        u.email,
        u.start_date,
        u.role,
        DATEDIFF(DATE(a.submissionTime), DATE(u.start_date)) as day_of_study
    FROM
        EmaAnswer a
            LEFT JOIN
        user u ON u.id = a.userId
            LEFT JOIN
        EmaSchedule s ON s.id = a.scheduleId
            LEFT JOIN
        Notification n ON n.id = a.notificationId
            LEFT JOIN
        EmaQuestion q ON q.id = a.questionId
            LEFT JOIN
        Activity ac ON ac.id = q.activityId
;
