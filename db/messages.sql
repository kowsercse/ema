INSERT INTO `Message` (`content`) VALUES('We are glad you are participating in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Thanks for participating in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Please keep working in your app – from the folks at Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Participating in Striving to Be Strong will help make your bone tissue flexible and strong – your bones thank you!');
INSERT INTO `Message` (`content`) VALUES('You can help your bones stay strong by getting the recommended about of calcium – from the folks at Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('You are forming new bone as you read this – how about a calcium rich snack today?');
INSERT INTO `Message` (`content`) VALUES('You are forming new bone as you read this – how about a bone building exercise?');
INSERT INTO `Message` (`content`) VALUES('Taking a walk is a great bone building activity – can you fit one in today?');
INSERT INTO `Message` (`content`) VALUES('Osteoporosis happens when you lose too much bone, make too little of it, or both. Striving to Be Strong is going to help you prevent that.');
INSERT INTO `Message` (`content`) VALUES('If a parent has broken a bone, you might be at risk for Osteoporosis. You can learn more about this in your Striving to Be Strong app.');
INSERT INTO `Message` (`content`) VALUES('Did you know about 9 million people in the US have osteoporosis? Congratulations on participating in Striving to Be Strong and trying to keep yourself out of that statistic!');
INSERT INTO `Message` (`content`) VALUES('Are you over 50 years old? You have a 1 in 2 chance to break a bone because of osteoporosis. Congratulations on participating in Striving to Be Strong to lower your risk!');
INSERT INTO `Message` (`content`) VALUES('As you get older and your estrogen levels decrease, your chances of developing osteoporosis increases.  Congratulations on participating in Striving to Be Strong to lower your risk!');
INSERT INTO `Message` (`content`) VALUES('Being lactose intolerant can make it hard to get enough calcium in your diet. Look for calcium rich foods in your Striving to Be Strong app.');
INSERT INTO `Message` (`content`) VALUES('Vitamin D helps you to absorb calcium which helps give you stronger bones. Have you gotten enough vitamin D today?');
INSERT INTO `Message` (`content`) VALUES('Fruits and vegetables benefit your overall health and your bones. How about a green leafy salad today?');
INSERT INTO `Message` (`content`) VALUES('Striving to Be Strong is helping you get strong bones.  What you are doing now might just be saving yourself from a broken bone later in life!');
INSERT INTO `Message` (`content`) VALUES('When a person has severe osteoporosis, a bone can be broken doing something as simple as sneezing, hugging, or bumping into furniture.  Your bones thank you for Striving to Be Strong!');
INSERT INTO `Message` (`content`) VALUES('Weight bearing exercise helps you build strong bones.  Find out more in your Striving to Be Strong app.');
INSERT INTO `Message` (`content`) VALUES('Do you know how much calcium you need every day?  Depending on your menopausal status, it is 1000 mg or 1200 mg. Read more about it in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Afraid of eating dairy because you are dieting? Try low fat yogurt to get more calcium in your diet.  You can find more suggestions in your Striving to Be Strong app.');
INSERT INTO `Message` (`content`) VALUES('Green vegetables that include calcium include broccoli, kale, and turnip greens.  Why don’t you eat some today?');
INSERT INTO `Message` (`content`) VALUES('You don’t have to do “organized” exercise to help your bones. Dance around your living room tonight to your favorite uptempo music and your bones will thank you!');
INSERT INTO `Message` (`content`) VALUES('Been hunting for a new sport to change up your routine? Striving to Be Strong has a lot of suggestions for new activities to get you moving.');
INSERT INTO `Message` (`content`) VALUES('Striving to Be Strong has balance exercises that will help you improve your bone health and reduce your risk of falls. Why not try one today?');
INSERT INTO `Message` (`content`) VALUES('Preventing falls is important to your bone health. Do you need a vision check-up? Why not schedule it today?');
INSERT INTO `Message` (`content`) VALUES('Preventing falls is important to your bone health.  Try removing all throw rugs that could move out from under you as you walk across them.');
INSERT INTO `Message` (`content`) VALUES('Preventing falls is important to your bone health.  Striving to Be Strong can help you improve your lower body strength.');
INSERT INTO `Message` (`content`) VALUES('One ounce of almonds, or approximately 24, contains 75 mg of calcium and is a tasty snack. Read more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('An eight ounce serving of fortified orange juice contains 350 mg of calcium and tastes great. Read more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Four ounces of fortified soy milk contains 225 mg of calcium and tastes great. Read more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Love coffee drinks? Ask your local barista if your drink is made with milk. Eight ounces contains 300 mg of calcium. Read more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('A slice of American cheese on your sandwich today will give you 143 mg of calcium. Read more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Have you tried Laughing Cow spreadable cheese?  One wedge packs 96 mg of calcium and tastes great! Read more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Taco night tonight? Shredded Mexican blend cheese has 185 mg of calcium in ¼ cup. Add some to your taco and make your bones happy. Read more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Tired of the same old snack at work? How about string cheese? One stick contains 180 mg of calcium. Read more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Love collard greens? So do your bones! 268 mg of calcium is in one cup of cooked collard greens. Read more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Looking for a cool treat today that is good for your bones? Soft-serve vanilla ice cream has 138 mg of calcium in a one cup serving. Read more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Sesame seeds are great in trail mix, on a salad, or enjoyed plain. They have 351 mg of calcium in each ¼ cup serving. Read more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Acorn squash has 90 mg of calcium in a one cup serving. Read more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('It is a myth that most people don’t need to worry about osteoporosis. By 2020, over half of all Americans over 50 will have either low bone density or osteoporosis. Your bones thank you for Striving to Be Strong!');
INSERT INTO `Message` (`content`) VALUES('It is a myth that you don’t need to worry about osteoporosis if you are over 50 and break a bone from a serious fall. Often broken bones are the first sign of low bone density. Your bones thank your for Striving to Be Strong!');
INSERT INTO `Message` (`content`) VALUES('It is a myth that people with osteoporosis can feel the bones getting weaker. That’s why it is called a “silent disease” – you might not know your bones are weak until you break one. Your bones thank you for Striving to Be Strong!');
INSERT INTO `Message` (`content`) VALUES('It is a myth that children and teens do not need to worry about their bone health.  You reach your peak bone mass between the ages of 18 and 25. Encourage your kids and grandkids to use the things you are learning in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('It is a myth that taking excessive calcium supplements will make your bones stronger.  Shoot for the recommended amount for your menopausal status daily – either 1000 mg or 1200 mg. Read Striving to Be Strong to learn more.');
INSERT INTO `Message` (`content`) VALUES('Maintaining changes requires consistent monitoring and thoughtful evaluation.  Keep up your work in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Begin again may happen many times as you try to improve your bone health – Keep Striving to Be Strong!');
INSERT INTO `Message` (`content`) VALUES('Striving to Be Strong means what it says – keep working at it and your bones will thank you.');
INSERT INTO `Message` (`content`) VALUES('It takes a lot of effort to make a behavior change. It takes even more effort to keep the change for a long time.  Keep up the good work – keep Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('You can become stronger. Learn more in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('It is often easier to remember a new behavior if you do it with something you have already been doing for a long time.  Try doing a strengthening exercise during each commercial during your favorite show. Read more suggestions in Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES(' Who doesn’t love a good story? Striving to Be Strong is full of great stories about women like you. Try reading one today in your app.');
INSERT INTO `Message` (`content`) VALUES('We believe in you and in the importance of your participation in Striving to Be Strong. Thank you.');
INSERT INTO `Message` (`content`) VALUES('Osteoporosis is not part of the normal aging process.  Your bones thank you for working to prevent it and Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Have you told a friend about Striving to Be Strong? You might be saving her from a broken bone later by telling her today!');
INSERT INTO `Message` (`content`) VALUES('You are an excellent role model to your daughters and granddaughters by Striving to Be Strong.');
INSERT INTO `Message` (`content`) VALUES('Want to learn more about your bone health and exercise safety? Check out the Striving (green) section of Striving to Be Strong.');
