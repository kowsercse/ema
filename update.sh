#!/usr/bin/env bash

git pull origin master
php app/console cache:clear --env=prod
php app/console assetic:dump --env=prod
sudo chmod -R 777 app/cache/ app/logs/
