<?php
function generateGiftCardReport(mysqli $conn, $start, $end, $giftCardRound, $roles) {
  global $databaseResults;
  $lastDate = 13*28 + 1;

  $totalSubmissionSql = "SELECT
    count(DISTINCT (a.submissionTime)) as submitted,
    DATE (u.start_date) as startDate,
    u.active as active,
    DATE_ADD(DATE(u.start_date), INTERVAL ($end) DAY) as completion,
    DATE_ADD(DATE(u.start_date), INTERVAL ($lastDate) DAY) as lastDate,
    u.*
  FROM EmaAnswer a
    LEFT JOIN user u ON u.id = a.userId
  WHERE
    a.submissionTime BETWEEN DATE_ADD(DATE(u.start_date), INTERVAL ($start) DAY) AND DATE_ADD(DATE(u.start_date), INTERVAL ($end) DAY)
    AND u.role IN ($roles)
  GROUP BY u.id
  ORDER BY u.role, u.name;";

  $result = $conn->query($totalSubmissionSql);
  while ($row = $result->fetch_assoc()) {
    $studyId = $row['name'];
    if(!array_key_exists($studyId, $databaseResults)) {
      $databaseResults[$studyId] = array();
    }
    $databaseResults[$studyId][$giftCardRound]['totalSubmission'] = $row;
  }


  $totalCancelled = "SELECT count(s.id) as cancelled, u.*
  FROM EmaSchedule s
    LEFT JOIN user u ON u.id = s.userId
  WHERE
    s.surveyDate BETWEEN DATE_ADD(DATE(u.start_date), INTERVAL ($start) DAY) AND DATE_ADD(DATE(u.start_date), INTERVAL ($end) DAY)
    AND s.denied = 1
    AND u.role IN ($roles)
  GROUP BY u.id
  ORDER BY u.role, u.name;";

  $result = $conn->query($totalCancelled);
  while ($row = $result->fetch_assoc()) {
    $studyId = $row['name'];
    $databaseResults[$studyId][$giftCardRound]['totalCancelled'] = $row;
  }
}

/**
 * @param array $databaseResult
 * @param $total
 * @param $totalCancelled
 */
function printProgress(array $databaseResult, $total, $totalCancelled,$studyId,mysqli $conn,$rnd) {

  if($rnd ==1)
    $sent = $conn->query("SELECT * FROM cards WHERE card_is_used='$studyId' AND reason='5' ");
  else if($rnd ==2)
    $sent = $conn->query("SELECT * FROM cards WHERE card_is_used='$studyId' AND reason='6' ");

  $sent = $sent->fetch_assoc();
  $sent = $sent['date_sent'];

  $totalSubmission = $databaseResult['totalSubmission']['submitted'] + $totalCancelled;
  $progress = 100*($totalSubmission) / (0.75 * $total);
  $actualProgress = 100*($totalSubmission) / ($total);
  echo "<td>" . ($totalSubmission) . "</td>";
  echo "<td>" . ($progress >= 100 ? 100.00 : sprintf('%0.2f', $progress)) . "</td>";
  echo "<td>" . ($progress >= 100 ? 100.00 : sprintf('%0.2f', $actualProgress)) . "</td>";
  echo "<td>" . ($progress >= 100 ? 'Yes' : 'No') . "</td>";
  echo "<td>".($progress >= 100 ? $sent : 'N/A') ."</td>";
}

/**
 * @param array $databaseResult
 */
function printSubmission(array $databaseResult, $total, $studyId,mysqli $conn,$rnd) {
  $totalCancelled = 0;
  if(array_key_exists('totalCancelled', $databaseResult)) {
    $totalCancelled = $databaseResult['totalCancelled']['cancelled'];
  }

  echo "<td>" . $databaseResult['totalSubmission']['submitted'] . "</td>";
  echo "<td>{$totalCancelled}</td>";
  printProgress($databaseResult, $total, $totalCancelled,$studyId,$conn,$rnd);
  echo "<td>" . $databaseResult['totalSubmission']['completion'] . "</td>";
}

function processRow($studyId, array $databaseResult,mysqli $conn) {
  echo "<td>$studyId</td>";
  echo "<td>{$databaseResult['first']['totalSubmission']['startDate']}</td>";
  echo "<td>{$databaseResult['first']['totalSubmission']['active']}</td>";

  $role = $databaseResult['first']['totalSubmission']['role'];
  printSubmission($databaseResult['first'], $role == 'Personal Choice Group' ? 84 : 196, $studyId,$conn,1);
  if(array_key_exists('second', $databaseResult)) {
    printSubmission($databaseResult['second'], $role == 'Personal Choice Group' ? 196 : 84,$studyId,$conn,2);
  }
  else {
    echo "<td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>No</td><td>N/A</td><td>{$databaseResult['first']['totalSubmission']['lastDate']}</td>";
  }
}


$conn = new mysqli("localhost", "user", "pass", "db");
if (!$conn) {
  die('Could not connect: ' . mysql_error());
}

global $databaseResults;
$databaseResults = array();
generateGiftCardReport($conn,           1,  3 * 28 + 1, "first",  "'SS Study Group', 'Boning Up Group'");
generateGiftCardReport($conn,  3 * 28 + 2, 13 * 28 + 1, "second", "'SS Study Group', 'Boning Up Group'");

generateGiftCardReport($conn,           1, 10 * 28 + 1, "first",  "'Personal Choice Group'");
generateGiftCardReport($conn, 10 * 28 + 2, 13 * 28 + 1, "second", "'Personal Choice Group'");

echo "<table border='1'>";
echo "<tr><td colspan='3'></td><td colspan='8'>First Round</td><td colspan='8'>Second Round</td></tr>";
echo "<tr><td>Study Id</td><td>Start Date</td><td>Active</td>
    <td>Submitted</td><td>Cancelled</td><td>Total</td><td>Progress</td><td>Actual Progress</td><td>Eligible</td><td>Send</td><td>Completion Date</td>
    <td>Submitted</td><td>Cancelled</td><td>Total</td><td>Progress</td><td>Actual Progress</td><td>Eligible</td><td>Send</td><td>Completion Date</td>";

ksort($databaseResults);
foreach($databaseResults as $studyId => $databaseResult) {
  echo "<tr>";
  processRow($studyId, $databaseResult,$conn);
  echo "</tr>";
}
echo "</table>";


$conn->close();
